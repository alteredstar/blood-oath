# coding=UTF-8

# General modules.
import os
import logging
from threading import Timer
import uuid
import time
import config
import json

# Tornado modules.
import tornado.ioloop
import tornado.web
import tornado.options
import sockjs.tornado
import tornado.escape

# Import application modules.
from gameplay import GamePlay
import model

users = {}
keys = {}
channels = {"lobby": []}
gameplay = GamePlay()

logging.basicConfig(filename=config.LOGGING['LOG_FILE'], level=logging.DEBUG, format=config.LOGGING['LOG_FORMAT'])


# logging.getLogger().setLevel(logging.DEBUG)

class IndexHandler(tornado.web.RequestHandler):
    """Regular HTTP handler to serve the chatroom page"""

    def get(self):
        self.render('index.html')


class ChatConnection(sockjs.tornado.SockJSConnection):
    """Chat connection implementation"""

    def subscribe(self, channel):
        logging.info("Subscribing to channel:" + channel)
        if channel not in channels:
            channels[channel] = []
        channels[channel].append(self)
        logging.info("Subscribed to channel:" + channel)

    def unsubscribe(self, channel):
        channels[channel].remove(self)

    def channel_send(self, msg):
        """
        called by game to push a message from the server to the channel subscribers.
        """
        logging.info('ChatSocketHandler.channel_send:' + str(msg))
        self.send(json.dumps(msg))

    def channel_broadcast(self, channel, msg):
        """
        called by game to push a message from the server to the channel subscribers.
        """
        logging.info('ChatSocketHandler.broadcast:' + str(msg))
        self.broadcast(channels[channel], json.dumps(msg))

    def on_open(self, info):
        # Create a redis connection for the current client
        logging.info('ChatSocketHandler.on_open')
        self.subscribe("lobby")

    def on_message(self, data):
        """
        Callback when new message received vie the socket.
        """
        logging.info('ChatSocketHandler.on_message')
        logging.info('Received new message %r', data)
        try:
            # Parse input to message dict.
            message = tornado.escape.json_decode(data)
            # Parse input again if it is still string :p (can happen for heavily encoded strings
            if not isinstance(message, dict):
                message = tornado.escape.json_decode(message)

            self.process_message(message)

        except Exception, err:
            # Send an error back to client.
            self.channel_send({'error': 5, 'textStatus': 'Bad input data ... ' + str(err) + data})
            return

    def on_close(self):
        """
        Callback when the socket is closed. Frees up resource related to this socket.
        """
        logging.info('ChatSocketHandler.on_close')
        logging.info("socket closed, cleaning up resources now")
        channels["lobby"].remove(self)

    def process_message(self, message):
        logging.info('ChatSocketHandler.process_message')
        # we would assume that once a user is logged in, there will always be a key in the command
        #  the key will be used to track user move.
        if 'command' in message:
            logging.info('-- Command Message: ' + message['command'] + ' --')
            if message['command'] == config.MESSAGES['CONNECT']:
                # Initialize the user account
                key = str(uuid.uuid1())

                # TODO: Verify the connect string key against a valid user in db
                data = message['data']
                if 'email' in data:
                    email = data['email']
                    if email not in users:
                        user = model.get_user(message['data']['email'])
                        if user is None:
                            logging.info('--First time ever user--')
                            new_user = {}
                            new_user['email'] = message['data']['email']
                            new_user['last_action'] = time.time()
                            new_user['key'] = key
                            new_user['nick'] = message['data']['nick']
                            new_user['clan'] = None
                            model.create_user(new_user)
                            users[email] = {}
                            users[email]['last_action'] = new_user['last_action']
                            users[email]['key'] = new_user['key']
                            users[email]['nick'] = new_user['nick'] #user_id
                            users[email]['clan'] = new_user['clan']
                            self.channel_send({"command": message['command'], "key": users[email]['key'],"comment":"A new User Created, Welcome to the Gang"})

                        else:
                            logging.info('-- Old but not loaded --')
                            users[email] = {}
                            users[email]['last_action'] = time.time()
                            users[email]['key'] = key
                            users[email]['nick'] = user['nick_name'] #user_id
                            users[email]['clan'] = user['clan_id']
                            self.channel_send({"command": message['command'], "key": users[email]['key'],"comment":"Old UserLoaded, Welcom Back"})
                    else:
                        logging.info('-- Old User --')
                        key = users[email]['key']

                    logging.info('-- User: ' + email + ' --')
                    keys[users[email]['key']] = email
                    #self.channel_send({"command": message['command'], "key": users[email]['key']})

                    logging.info('-- Key: ' + users[email]['key'] + ' --')
                    self.subscribe("global_chat")
                    if users[email]['clan'] is not None:
                        self.subscribe(users[email]['clan'])
                else:
                    logging.info('-- No Nick + User ID --')
                    self.channel_send({'error': 2, 'textStatus': 'WOW! No Nick!? '})
            else:
                if message['command'] == config.MESSAGES['DISCONNECT']:
                    key = message[u'key']
                    user = users[keys[key]]
                    self.channel_send({"command": message['command'],"comment":"Disconnecting, bhag ja yaha se"})
                    self.unsubscribe("global_chat")
                    self.unsubscribe(user['clan'])
                    users.pop(keys[key])
                    keys.pop(key)
                else:
                    if 'key' in message:
                        key = message[u'key']
                        if key in keys:
                            user = users[keys[key]]
                            user[u'last_action'] = time.time()

                            logging.info('-- User: ' + user['nick'] + ' --')
                            logging.info('-- Key: ' + key + ' --')
                            # once the common housekeeping actions are done. Delegate the processing to the gameplay class
                            getattr(gameplay, message['command'])(self, message['data'], user)
                        else:
                            logging.info('-- Invalid Key --')
                            self.channel_send({'error': 3, 'textStatus': 'WOW! Invalid Key!? '})
                    else:
                        logging.info('-- No Key --')
                        self.channel_send({'error': 3, 'textStatus': 'WOW! No Key!? '})

        else:
            logging.info('-- No Command --')
            self.channel_send({'error': 4, 'textStatus': 'WOW! No Command!? '})


def main():
    """
    Main function to run the chat application.
    """

    logging.info("Server Reading!")
    ChatRouter = sockjs.tornado.SockJSRouter(ChatConnection, '/socket', user_settings={"disconnect_delay": 3600})

    # 2. Create Tornado application
    app = tornado.web.Application([(r"/", IndexHandler)] + ChatRouter.urls)
    if (os.path.isfile('/yo') and os.path.exists('/yo')):
        tornado.options.parse_config_file('/yo', False)
    app.listen(9002)
    logging.info("Server Ready!")
    tornado.ioloop.IOLoop.instance().start()


if __name__ == "__main__":
    main()
