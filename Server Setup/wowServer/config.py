class GAME_STATUS:
	NONE = 0
	SOCKET_DISCONNECTED = 1
	SOCKET_CONNECTED = 2
	CONNECT_SENT = 3
	USER_KEY_RECEIVED = 4
	START_GAME_SENT = 5
	WAITING_FOR_START = 6
	PLAYING = 10
	GAME_OVER = 20


MESSAGES = {'CONNECT': 'connect',
            'DISCONNECT':'disconnect',
            'REMATCH': 'rematch',
            'END_GAME': 'end_game',
            'GAME_OVER': 'game_over',
            'Clan_Chat': 'clan_chat'}
COOKIE_SECRET = '43osdETzKXasdQAGaYdkL5gEmGeJJFuYh7EQnp2XdTP1o/Vo='

DATABASE = {
    "DATABASE": "BloodOath",
    "USER": "root",
    "PASSWORD": "1234",
    "HOST": "localhost",
    "PORT": 3306
}

ERROR_CODES = {
    "GAME_EXPIRED": "100"
}

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S.%f"
FRENZY_SCORE = 1
FRENZY_DURATION = 15
WORD_FIND_DURATION = 10
MAX_WORDS_TO_START_FRENZY = 3

TIMER_STATE = {
    "EXPIRED": 0,
    "ACTIVE_NON_FRENZY": 1,
    "ACTIVE_FRENZY": 2
}

LOGGING = {
    "LOG_FILE": "wow_server.log",
    "LOG_FORMAT": "%(asctime)s %(message)s"
}

#TESTCOMMENT