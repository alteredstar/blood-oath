__author__ = 'omersaeed'

# Legend of the board ! ! ! ! !
# _ means board cell is permanently irrevokably blocked. CANNOT be used EVER
# + this is an intermediate state that allows the rest of the board making modes to use the cell of the frame
# 0 means regular cell, that will be filled by a character with the given score (0_9) of course
# ? means a cell that will take a PERMANENT state once it's used
# * this is a wild card. Will always remain as a wild card regardless of how many different characters use it.
# . hidden message char. there is a fixed character underneath it that will NEVER be shown
#   even after the correct word is matched around it.
# For the board the probability sets are defined like this
#	 [
#		 {u"symbol": '1', "count": 2, u"min_size": 2, u"max_size": 4},
#		 {u"symbol": '2', "count": 2, u"min_size": 2, u"max_size": 3},
#		 {u"symbol": '3', "count": 2, u"min_size": 2, u"max_size": 4},
#		 {u"symbol": '?', "count": 4, u"min_size": 1, u"max_size": 1},
#		 {u"symbol": '*', "count": 3, u"min_size": 1, u"max_size": 1}
#	 ]


import time
import json
import uuid
import random
import model
import config
from datetime import datetime, timedelta
import logging
from operator import itemgetter

# logging config
logging.basicConfig(filename=config.LOGGING['LOG_FILE'], level=logging.DEBUG, format=config.LOGGING['LOG_FORMAT'])


class GamePlay():
    """
	A base request Handler providing user authentication.
	It also provides a render_default() method which passes arguments to render()
	with additional default arguments for the menu, user...
	"""

    def __init__(self):
        self.games = {}
        self.open_games = []
        self.full_games = []
        self.finished_games = []

    @staticmethod
    def all_words_found(game):
        all_words_found = True
        for word in game['displayed_words']:
            if word not in game['found_words']:
                all_words_found = False
                break

        return all_words_found

    def refresh_wait(self, game):
        print "Get players from database and broadcast on channel."

    def random_taunt(self, app, message, user):
        app.channel_broadcast("global_chat", {'command': 'random_taunt', 'taunt': message["taunt"]})

    def global_chat(self, app, message, user):
        app.channel_broadcast("global_chat", {'command': 'global_chat', 'data': user['nick']+' : '+message})

    def clan_chat(self, app, message, user):
        if(user['clan'] is None):
            app.channel_send({'command': 'clan_chat', 'data': 'You dont belong to a clan yet'})
        else:
            app.channel_broadcast(user['clan'], {'command': 'clan_chat', 'data': user['nick']+' : '+ message})

    def create_clan(self, app, message, user):
        new_clan = {}
        new_clan['clan_name'] = message['clan_name']
        new_clan['user'] = user
        model.create_clan(new_clan)
        app.channel_send({'command': 'clan_chat', 'data': user['nick']+' : My own clan'})

    def clan_joining(self, app, message, user):
        new_clan = {}
        new_clan['clan_id'] = message['clan_id']
        new_clan['user'] = user
        model.clan_joining(new_clan)
        app.subscribe(new_clan['clan_id'])
        app.channel_broadcast(new_clan['clan_id'], {'command': 'clan_chat', 'data': user['nick']+' has Joined the Clan, Cheers!!'} )
        new_clan = {}
        new_clan['clan_id'] = message['clan_id']
        new_clan['user'] = user
        model.clan_joining(new_clan)
        app.subscribe(new_clan['clan_id'])



    def search_clan(self, app, message, user):
       clans = model.search_clan(message)

       app.channel_send({"command": "search_clan","clans":clans})