import sqlalchemy.pool as pool
from datetime import datetime
import config
import mysql.connector
import logging
import config
import uuid

#logging config
logging.basicConfig(filename=config.LOGGING['LOG_FILE'], level=logging.DEBUG, format=config.LOGGING['LOG_FORMAT'])

#PostGRESql
def connect_db():
	c = None
	try:
		DATABASE = config.DATABASE
		c = mysql.connector.connect(database=DATABASE["DATABASE"], user=DATABASE["USER"], host=DATABASE["HOST"], port=DATABASE["PORT"], password=DATABASE["PASSWORD"])
	except mysql.connector.DatabaseError, e:
		print 'Error %s' %e
	finally:
		return c

db_connection_swimming_pool = pool.QueuePool(connect_db , max_overflow=10, pool_size=5)


def get_connection_from_the_swimming_pool():
	con = db_connection_swimming_pool.connect()
	return con


def release_connection_back_to_the_swimming_pool(con):
	if con:
		con.close()

def get_user(email):
	con = None
	try:
		con = get_connection_from_the_swimming_pool()
		cur = con.cursor(dictionary=True)
		cur.execute('''
			SELECT	*
			FROM users where email_address = %s
			''',[email])

		return cur.fetchone()
	except mysql.connector.DatabaseError, e:
		if con:
			release_connection_back_to_the_swimming_pool(con)
		raise e
	finally:
		release_connection_back_to_the_swimming_pool(con)

def create_user(user):
	con = None
	try:
		con = get_connection_from_the_swimming_pool()
		cur = con.cursor(dictionary=True)
		cur.execute('''
			INSERT INTO `bloodoath`.`users`
			(`nick_name`,
			`email_address`)
			VALUES
			(%s,
			 %s);
			''',[user['nick'],user['email']])

		con.commit()
	except mysql.connector.DatabaseError, e:
		if con:
			con.rollback()
			release_connection_back_to_the_swimming_pool(con)
		raise e
	finally:
		release_connection_back_to_the_swimming_pool(con)

def create_clan(clan):
	con = None
	try:
		clan_id = str(uuid.uuid1())
		con = get_connection_from_the_swimming_pool()
		cur = con.cursor(dictionary=True)
		cur.execute('''
			INSERT INTO `bloodoath`.`clans`
			(`clan_id`,
			`clan_name`
			)
			VALUES
			(%s,
			 %s);
			''',[clan_id,clan['clan_name']])

		cur.execute('''
		    UPDATE `bloodoath`.`users`
		    SET `clan_id` = %s,
		    `clan_role` = %s
		    Where `key` = %s
		''',[clan_id,'Leader',clan['user']['key']])

		con.commit()
	except mysql.connector.DatabaseError, e:
		if con:
			con.rollback()
			release_connection_back_to_the_swimming_pool(con)
		raise e
	finally:
		release_connection_back_to_the_swimming_pool(con)

def clan_joining(clan):
	con = None
	try:
		con = get_connection_from_the_swimming_pool()
		cur = con.cursor(dictionary=True)
		cur.execute('''
		    UPDATE `bloodoath`.`users`
		    SET `clan_id` = %s,
		    `clan_role` = %s
		    Where `key` = %s
		''',[clan['clan_id'],'Leader',clan['user']['key']])

		con.commit()
	except mysql.connector.DatabaseError, e:
		if con:
			con.rollback()
			release_connection_back_to_the_swimming_pool(con)
		raise e
	finally:
		release_connection_back_to_the_swimming_pool(con)

def search_clan(clan):
	con = None
	try:
		con = get_connection_from_the_swimming_pool()
		cur = con.cursor(dictionary=True)
		cur.execute('''
		    Select *
		    from clans
		    where clan_name = %s
		''',[clan])

		return cur.fetchone
	except mysql.connector.DatabaseError, e:
		if con:
			con.rollback()
			release_connection_back_to_the_swimming_pool(con)
		raise e
	finally:
		release_connection_back_to_the_swimming_pool(con)