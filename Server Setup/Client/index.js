// Create a connection to http://localhost:9999/echo
var sock = new SockJS('http://localhost:9002/socket');
var key ='';

// Open the connection
sock.onopen = function() {
  console.log('open');
};

// On connection close
sock.onclose = function() {
  console.log('close');
};

// On receive message from server
sock.onmessage = function(e) {
  // Get the content
  var content = JSON.parse(e.data);

  // Append the text to text area (using jQuery)
    if(content.error != undefined) {
        $('#errorlog').val(function(i, text){
            return text + '\n [' + content.error + ']: ' + content.textStatus + '\n';
        });
    } else {
      // Valid Server responses come here
      if(content.command == "connect") {
        $('#connectioninfo').val(function(i, text){
          text += '\n [ command ]: ' + content.command + '\n' ;
          text += '[ key ]: ' +''+content.key+'\n' ;
          text += content.comment + '\n' ;
          return text;
        });
        key = content.key;
      }
      if(content.command == "disconnect") {
        $('#connectioninfo').val(function(i, text){
          text += '\n [ command ]: ' + content.command + '\n' ;
          text += content.comment + '\n' ;
          return text;
        });
        key = '';
      }
      if(content.command == "global_chat") {
        $('#GC').val(function(i, text){
          return text + content.data + '\n';
        });
      }

      if(content.command == "clan_chat") {
        $('#CC').val(function(i, text){
          return text + content.data + '\n';
        });
      }
      if(content.command == "search_clan") {
        $('#ClanSearchRes').val(function(i, text){
          return text + content.clans + '\n';
        });
      }


    }

};

function startConnection()
{
    var email = $('#Email').val();
    var nick = $('#Nick').val();

    if(!isValidEmailAddress(email))
    {
       $('#errorlog').val(function(i, text){
            return text + '\n Invalid Email Address \n';
        });
        return;
    }

  // Get the content from the textbox
    var connect_command = '{"command": "connect", "data": {"email": "'+email+'","nick":"'+nick+'"} }';
    sock.send(JSON.stringify(connect_command));
}

function EndConnection()
{
  var msg = '{"command": "disconnect","key":"'+key+'"}'
  sock.send(JSON.stringify(msg));
}

function isValidEmailAddress(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
};

// Function for sending the message to server
function sendGlobalMessage(){
  // Get the content from the textbox
  var msg = '{"command": "global_chat","key":"'+key+'","data":"'+$('#GCtext').val()+'" }'
  sock.send(JSON.stringify(msg));
}

function sendClanMessage(){
  // Get the content from the textbox
  var msg = '{"command": "clan_chat","key":"'+key+'","data":"'+$('#CCtext').val()+'" }'
  sock.send(JSON.stringify(msg));
}

function SearchClan(){
  // Get the content from the textbox
  var msg = '{"command": "search_clan","key":"'+key+'","data":"'+$('#ClanSearch').val()+'" }'
  sock.send(JSON.stringify(msg));
}
function createClan(){
  // Get the content from the textbox
  var msg = '{"command": "create_clan","key":"'+key+'","data":{"clan_name":"'+$('#ClanName').val()+'"} }'
  sock.send(JSON.stringify(msg));
}

function clanJoining(){
  // Get the content from the textbox
  var msg = '{"command": "clan_joining","key":"'+key+'","data":{"clan_id":"'+$('#ClanId').val()+'"} }'
  sock.send(JSON.stringify(msg));
}

function executecommand(){
  // Get the content from the textbox
  sock.send(JSON.stringify($('#ccommand').val()));
}