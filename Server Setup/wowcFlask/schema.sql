-- for FB App
create table player (
  -- id integer primary key,
  id bigint primary key,
  username text,
  access_token text,
  total_points integer
);

create table game (
  -- id integer primary key autoincrement,
  id serial primary key,
  start_time timestamp,
  end_time timestamp,
  game_board text
);

create table player_game (
  -- user_id integer,
  user_id bigint references player(id),
  game_id integer references game(id),
  score integer
);

create table gamelevel (
	id serial primary key,
	name text
);

create table player_gamelevel (
	user_id bigint references player(id),
	level_id integer references gamelevel(id),
	status text,
	score integer
);
-- create table feedback (
--  rating double,
--  comments text,
--  feedback_time timestamp,
--  user_id integer
-- );
