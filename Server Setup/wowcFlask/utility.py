__author__ = 'salmanqureshi'

from base64 import urlsafe_b64decode
from constants import 	GAME_OVER_REASON as game_end_reason, \
						LEVEL_STATUS as level_status, \
						EXCEPTION as exception
import json
import facebook
import hmac
from hashlib import sha256
import logging

class Utility:

	@staticmethod
	def reason_to_message(reason, score, level_meta):
		status = "Failed"
		message = "Give it another try!"
		min_pass_target = level_meta["score_target"] * level_meta["star1"] / 100
		if reason == game_end_reason.FOUND_ALL_WORDS:
			if score >= min_pass_target:
				# TARGET ACHIEVED
				status = level_status.COMPLETED
				message = "You did it!"
			else:
				# TARGET NOT ACHIEVED
				status = level_status.FAILED
				message = "You did not get enough score!"
		elif reason == game_end_reason.TIMED_OUT:
			if score >= min_pass_target:
				# TARGET ACHIEVED
				status = level_status.COMPLETED
				message = "You can do better!"
			else:
				# TARGET NOT ACHIEVED
				status = level_status.FAILED
				message = "You ran out of time!"
		elif reason == game_end_reason.KILL_MINUS_NINE:
			if score >= min_pass_target:
				# TARGET ACHIEVED
				status = level_status.COMPLETED
				message = "You gave up, but still finished on top!"
			else:
				# TARGET NOT ACHIEVED
				status = level_status.FAILED
				message = "You chickened out!"
		return {
			"status": status,
			"message": message
		}

	@staticmethod
	def base64_url_decode(input):
		input += '=' * (4 - (len(input) % 4))
		return urlsafe_b64decode(input.encode('utf-8'))

	@staticmethod
	def parseFacebookRequest(app, signed_request, request_id, alternate_oauth_token):
		gameKey = ''
		try:
			fb_app_secret = app.config['FB_APP_SECRET']

			#parse and decode signed request
			[encoded_sig, payload] = signed_request.split('.')

			# decode data
			sig = Utility.base64_url_decode(encoded_sig)
			data = json.loads(Utility.base64_url_decode(payload))

			if data['algorithm'].upper() != app.config['FB_DECODE_ALGO']:
				raise Exception(exception.FACEBOOK_UNKNOWN_ALGORITHM)

			# check sig
			expected_sig = hmac.new(fb_app_secret, payload, sha256).digest()

			if sig != expected_sig:
				raise Exception(exception.FACEBOOK_INVALID_SIGNATURE)

			profile = None
			#parse request id's to get gamekey
			if "oauth_token" in data:
				graph = facebook.GraphAPI(data["oauth_token"])
				profile = graph.get_object('me')
			elif alternate_oauth_token:
				graph = facebook.GraphAPI(alternate_oauth_token)
				profile = graph.get_object('me')

			if request_id and profile:
				# temp_request_ids = request_values['request_ids']
				# request_ids = temp_request_ids.split(',')
				# req_index = len(request_ids) - 1
				# request_id = request_ids[req_index]
				try:
					gameKey = graph.get_object(request_id)['data']
					# graph.delete_request(profile["id"], request_id)
				except Exception as e:
					raise Exception(exception.FACEBOOK_DELETED_REQUEST)

					'''
					for request_id in request_ids:
						if graph.get_object(request_id).has_key('data'):
							gamekey = graph.get_object(request_id)['data']
						#graph.delete_request(u_id,request_id)
					'''
		except Exception as e:
			logging.exception('Error while retrieving game key from FB request %s', e)
			raise e

		return gameKey