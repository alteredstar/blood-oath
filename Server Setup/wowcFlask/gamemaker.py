__author__ = 'omersaeed'

# Legend of the board ! ! ! ! !
# - means board cell is permanently irrevokably blocked. CANNOT be used EVER
# + this is an intermediate state that allows the rest of the board making modes to use the cell of the frame
# ? means a cell that will take a PERMANENT state once it's used
# * this is a wild card. Will always remain as a wild card regardless of how many different characters use it.
# . hidden message char. there is a fixed character underneath it that will NEVER be shown even after the correct word is matched around it.
# For the board the probability sets are defined like this
#     [
#         {u"symbol": u'1', "count": 2, u"min_size": 2, u"max_size": 4},
#         {u"symbol": u'2', "count": 2, u"min_size": 2, u"max_size": 3},
#         {u"symbol": u'3', "count": 2, u"min_size": 2, u"max_size": 4},
#         {u"symbol": u'?', "count": 4, u"min_size": 1, u"max_size": 1},
#         {u"symbol": u'*', "count": 3, u"min_size": 1, u"max_size": 1}
#     ]


import copy
import random
import string
import uuid
import config
import logging

# To stack dictionaires and speed things up.
dictionaries = {}
valid_cells = [u'+', u'?', u'*']
directions = [(0, 1), (1, 0), (0, -1), (-1, 0), (1, 1), (-1, -1), (-1, 1), (1, -1)]


class GameMaker():
    """
    A base request Handler providing user authentication.
    It also provides a render_default() method which passes arguments to render()
    with additional default arguments for the menu, user...
    """

    def __init__(self):
        pass

    def paint_block(self, frame, rows, cols, symbol, count, min_size, max_size):
        block_id = 0
        changes = True
        while block_id < count:
            if changes:
                block_id += 1

            changes = False
            block_length_x = random.randint(min_size, max_size)
            block_length_y = random.randint(min_size, max_size)
            position_x = random.randint(0, rows - 1)
            position_y = random.randint(0, cols - 1)
            for x in range(position_x, position_x + block_length_x):
                for y in range(position_y, position_y + block_length_y):
                    if x < rows and y < cols:
                        if frame[x][y] == '+':
                            frame[x][y] = symbol
                            changes = True

    def make_frame(self, rows, cols, num_blocks, min_size, max_size):
        frame = [['+' for i in xrange(cols)] for i in xrange(rows)]
        self.paint_block(frame, rows, cols, '-', num_blocks, min_size, max_size)
        frame_data = {}
        frame_data[u'uuid'] = str(uuid.uuid1())
        frame_data[u'map'] = frame
        return frame_data

    def make_board(self, frame, probability_set):
        board = copy.deepcopy(frame[u'map'])
        rows = len(board)
        cols = len(board[0])
        for set in probability_set:
            self.paint_block(board, rows, cols, set[u'symbol'], set[u'count'], set[u'min_size'], set[u'max_size'])

        board_data = {}
        board_data[u'uuid'] = str(uuid.uuid1())
        board_data[u'frame'] = frame
        board_data[u'map'] = board
        return board_data

    def get_dictionary(self, dictionary):
        return self.make_dictionary(dictionary)

    def make_dictionary(self, dictionary):
        dict = []
        lines = [line.strip() for line in open("%s" % dictionary)]

        for line in lines:
            logging.info(line)
            (word, hint, meaning) = line.split('\t')
            dict.append((string.lower(word.strip()).decode('utf8'), "" + string.lower(hint.strip()).decode('utf8'),
                         "" + meaning.strip().decode('utf8')))

        return dict

    def make_game(self, board, maxwords, minwordlength, maxwordlength, dictionary):

        if dictionary not in dictionaries:
            dictionaries[dictionary] = copy.deepcopy(self.get_dictionary("../dictionaries/" + dictionary))

        # Prepare Dictionary as a randomized list
        dict = copy.copy(dictionaries[dictionary])
        random.shuffle(dict)
        game = copy.deepcopy(board[u'map'])

        # Setup local vars that hold the words used.
        used_words = ""
        included_words = {}

        # prepare a list of valid start positions.
        rows = len(game)
        cols = len(game[0])
        rc_tuples = []
        for row in range(0, len(game)):
            for col in range(0, len(game[0])):
                rc_tuples.append((row,col))

        # start the exhaustive random search

        for word_set in dict:
            is_valid = True
            word = word_set[0]
            if len(word) >= minwordlength and len(word) <= maxwordlength:

                word_directions = copy.copy(directions)
                random.shuffle(word_directions)
                is_valid = True
                for d in word_directions:

                    rc = copy.copy(rc_tuples)
                    random.shuffle(rc)
                    is_valid = True
                    for (r,c) in rc:

                        i = 0
                        (cur_row, cur_col) = (r, c)
                        is_valid = True
                        while i < len(word) and is_valid:

                            if cur_row < 0 or cur_row >= rows or cur_col < 0 or cur_col >= cols:
                                is_valid = False
                                break

                            if game[cur_row][cur_col] in valid_cells or game[cur_row][cur_col] == word[i]:
                                i += 1
                                cur_row += d[0]
                                cur_col += d[1]
                            else:
                                is_valid = False
                                break

                        if is_valid and i == len(word):
                            (cur_row, cur_col) = (r, c)
                            for i in xrange(len(word)):
                                if game[cur_row][cur_col] != u'*':
                                    game[cur_row][cur_col] = word[i]
                                cur_row += d[0]
                                cur_col += d[1]

                            included_words[word_set[0]] = (word_set[1], word_set[2])
                            used_words += word
                            maxwords -= 1
                            break
                        else:
                            is_valid = False

                    if is_valid:
                        break

            if maxwords == 0:
                break

        if maxwords == 0:
            for i in xrange(rows):
                for j in xrange(cols):
                    if game[i][j] in valid_cells and game[i][j] != u'*':
                        game[i][j] = random.choice(used_words)

            # prepare and return the game dict
            game_data = {}
            game_data[u'uuid'] = str(uuid.uuid1())
            game_data[u'board'] = board
            game_data[u'map'] = game
            game_data[u'words'] = included_words
            return {u"game": game_data}
        else:
            raise Exception("Board Could Not Be Generated!!!!")

    def get_game(self, data):
        # TODO: The system may choose to return an old dict based on these params as well.
        frame = self.make_frame(data['rows'], data['cols'], 0, 0, 0)
        board = self.make_board(frame, [])
        dictionary = data['dictionary']
        game = self.make_game(board, data['max_words'], data['min_length'], data['max_length'], dictionary)
        return game
