$(document).ready(function() {

	$(document).on("click", ".fbLogin", function() {
		playAudio(GC.BUTTON_CLICKED_AUDIO);
		connectFacebook(false); //ignore popup blocking. User initiated FB login.
	});

	$("#btnFacebookLogout").click(function(){
		playAudio(GC.BUTTON_CLICKED_AUDIO);
		disconnectFacebook();
	});

	$(document).on("click", ".levelButton, .playAgainButton", function() {
		var child = $(this).find('[data-level_id]');
		var level = $(child).data('level_id');
		if (level != undefined) {
			playAudio(GC.BUTTON_CLICKED_AUDIO);
			ZeCommunicator.loadLevel(level);
		}
	});

	$(document).on("click", "div#campaignScreen .multiplayerItem", function() {
		playAudio(GC.CHALLENGE_AUDIO);
		GV.isSinglePlayer = false;
		ZeCommunicator.loadMultiplayerScreen();
	});

	$(document).on("click", ".backButton", function() {
		playAudio(GC.BUTTON_CLICKED_NEGATIVE_AUDIO);
		var id = $(this).attr('id');
		handleBackButton(id);
	});

	$(document).on("click", ".campaignItem, .nextLevelButton", function() {
		playAudio(GC.BUTTON_CLICKED_AUDIO);
		ZeCommunicator.loadPlayerCampaign($(this).data('campaign_id'));
	});

	$(document).on("click", ".startGame", function() {
		playAudio(GC.BUTTON_CLICKED_AUDIO);
		ZeMultiCommunicator.force_start();
	});

	$(document).on("click", ".fbInviteButton", function() {
		playAudio(GC.BUTTON_CLICKED_AUDIO);
		savePlayerInvitations(true);
	});

	$(document).on("click", ".fbRequestButton", function() {
		playAudio(GC.BUTTON_CLICKED_AUDIO);
		sendGameRequests(true);
	});

	$(document).on("click", ".fbShareButton", function() {
		playAudio(GC.BUTTON_CLICKED_AUDIO);
		FB.ui({
			method: 'share',
			href: 'https://developers.facebook.com/docs/',
		}, function(response){});
	});

	$(document).on("click", ".quitButton", function() {
		playAudio(GC.BUTTON_CLICKED_NEGATIVE_AUDIO);
		if ($(this).data('scene_name') === "END") {
			ZeCommunicator.getOrCreatePlayer(GV.playerCache.facebook.accessToken);
		} else if ($(this).data('scene_name') === "GAME") {
			if (confirm("Are you sure you want to leave the game?")) {
				gameOver(GC.GameOverReason.KILL_MINUS_NINE);
			}
		}
	});


//	bindTouchHandlers();
	initUI();
	userVoiceIntegration();

});

function handleBackButton(divID) {
	if (divID === "backOnCampaign") {
		ZeCommunicator.loadAllCampaigns();
	}
}

function show_message(msg) {
	/*
	if (!msg || msg=='') {
	$("#message").hide();
	return
	}
	*/
	$("#message").show();
	$("#messageDiv").show();
	$("#message").html(msg);
}

function bindTouchHandlers() {
	var isIOS = ((/iphone|ipad/gi).test(navigator.appVersion));
	//Process Down
	$('#canvas').bind('touchstart', function(e){
		processDown(e);
	});
	$('#canvas').bind('mousedown', function (e) {
		if(!isIOS) processDown(convertToTouch(e));
	});

	//Process Up
	$('#canvas').bind('touchend', function(e){
		processUp(e);
	});
	$('#canvas').bind('mouseup', function (e) {
		if(!isIOS) processUp(convertToTouch(e));
	});

	//Process Move
	$('#canvas').bind('touchmove', function(e){
		processMove(e);
	});
	$('#canvas').bind('mousemove', function (e) {
		processMove(convertToTouch(e));
	});


	$("#canvas").bind('touchcancel', function (e) {
		processOut(e);
	});
	$("#canvas").bind('mouseout', function (e) {
		processOut(convertToTouch(e));
	});
}

function unbindTouchHandlers(){
	$('#canvas').unbind('touchmove');
	$('#canvas').unbind('mousemove');
	$("#canvas").unbind('touchcancel');
	$("#canvas").unbind('mouseout');
	$('#canvas').unbind('touchstart');
	$('#canvas').unbind('mousedown');
	$('#canvas').unbind('touchend');
	$('#canvas').unbind('mouseup');
}

function initUI() {
	rematch = false;
	lineBeingDrawn = false;
	isTouchDown = false;
	$("#splash").show();
	$("#pnlStart").hide();
	$("#messageDiv").hide();
	$("#rematch").hide();
	$("#wallPost").hide();
	$("#endGame").hide();

	//ELEMENTS FOR CREATING A GAME
	$("#selectGameCategory").hide();
	$("#proceedToGame").hide();

	$("#startScreen").show();

	$("#frenzyProgressBar").width('0%');

	if (navigator.userAgent.search("Firefox") >= 0) {
		$("#timerSpan").css("padding", "0px 2px 0px 0px");
	}

	isCreator = true;
	GV.totalWords = 0;
	GV.gameState = GC.GameState.NONE;
	pulse = false;


	/*
	game = undefined;
	GV.userKey = '';
	*/
}


/* TOUCH HANDLERS */
//touch start and end coordinates
var startX, startY, endX, endY;
//flag to check if the line is being drawn or not
var lineBeingDrawn = false;
//Is touch down?
var isTouchDown = false;

function convertToTouch(e) {
	e.originalEvent.changedTouches = [{clientX: e.clientX, clientY: e.clientY}];
	return e;
}

function processDown(e) {
	if(typeof GV.gameCache === 'undefined') {
		return false;
	}

	isTouchDown = true;
	startX = endX = e.pageX - GV.canvasOffsetX;
	startY = endY = e.pageY - GV.canvasOffsetY;

	//if touch is outside the bound then ignore it.
	if (startX<0 || Math.floor(startX/GV.tileWidth)>GV.gridColumns || startY<0 || Math.floor(startY/GV.tileWidth)>GV.gridRows) {
		lineBeingDrawn = false;
		return false;
	}
	lineBeingDrawn = true;
	//start
	board.drawGameCanvas("canvas2");
}

function processMove(e) {
	e.preventDefault();
	if(typeof GV.gameCache === 'undefined' || GV.gridColumns == -1) {
		return false;
	}

	newEndX = e.pageX - GV.canvasOffsetX;
	newEndY = e.pageY - GV.canvasOffsetY;

	if (newEndX>=0 && Math.floor(newEndX/GV.tileWidth)<=GV.gridColumns && newEndY>=0 && Math.floor(newEndY/GV.tileWidth)<=GV.gridRows && isTouchDown) {
		lineBeingDrawn = true;
	} else {
		lineBeingDrawn = false;
	}

	if (!lineBeingDrawn) {
		return false;
	}

	if (newEndX<0) {
		newEndX=0;
	}
	if (Math.floor(newEndX/GV.tileWidth)>GV.gridColumns) {
		newEndX= ((GV.gridColumns+1)*GV.tileWidth)-1;
	}
	if (newEndY<0) {
		newEndY = 0;
	}
	if (Math.floor(newEndY/GV.tileWidth)>GV.gridRows) {
		newEndY = ((GV.gridRows+1)*GV.tileWidth)-1;
	}
	endX = newEndX;
	endY = newEndY;

	if (startX<0 || Math.floor(startX/GV.tileWidth)>GV.gridColumns || startY<0 || Math.floor(startY/GV.tileWidth)>GV.gridRows) {
		startX = endX;
		startY = endY;
	}
	board.drawGameCanvas("canvas2");
	if(lineBeingDrawn) {
		drawLine();
	}
}

function processUp(e) {
	if(typeof GV.gameCache === 'undefined')
		return false;

	isTouchDown = false;
	lineBeingDrawn = false;
	endX = e.pageX - GV.canvasOffsetX;
	endY = e.pageY - GV.canvasOffsetY;

	if (endX<0) {
		endX=0;
	}
	if (Math.floor(endX/GV.tileWidth)>GV.gridColumns) {
		endX=((GV.gridColumns+1)*GV.tileWidth)-1;
	}
	if (endY<0) {
		endY = 0;
	}
	if (Math.floor(endY/GV.tileWidth)>GV.gridRows) {
		endY = ((GV.gridRows+1)*GV.tileWidth)-1;
	}

	//update end point to get correct word coordinates to mark and send word highlighted by auto aligned mark.
	newPt = closestValidPoint(startX,startY,endX,endY);
	endX = newPt.x;
	endY = newPt.y;
	drawLine();

	board.drawGameCanvas("canvas2");
	drawTempLine();
	move(	Math.floor(startX/GV.tileWidth), Math.floor(startY/GV.tileWidth),
			Math.floor(endX/GV.tileWidth), Math.floor(endY/GV.tileWidth) );
}

function move(x1, y1, x2, y2) {
	game_map = GV.gameCache.game
	dx = 0
	dy = 0

	if (x1 < x2) dx = 1;
	if (x1 > x2) dx = -1;
	if (y1 < y2) dy = 1;
	if (y1 > y2) dy = -1;
	var word = '';
	x = x1; y = y1;	ci = 0; len = Math.max(Math.abs(x2-x1),Math.abs(y2-y1));
	while (ci <= len) {
		if(x>GV.gridColumns || y>GV.gridRows || x<0 || y<0) {
			return false;
		}
		/*
		* In 2D array world the orientation of x and y is reversed from what we show
		* on the front end. So moving along x means change in columns
		* and moving along y means we are changing rows.
		*/
		word += game_map[y][x];
		x += dx;
		y += dy;
		ci += 1;
	}

	if (GV.isSinglePlayer) {
		checkMove(y1, x1, y2, x2, word, FrenzyKeeper.frenzyState);
		board.displayLevel();
		if (GV.gameState == GC.GameState.GAME_OVER) {
			gameOver(GC.GameOverReason.FOUND_ALL_WORDS);
		}
	} else {
		ZeMultiCommunicator.move(GC.COMMANDS.move, GV.userKey, x1, y1, x2, y2, word, FrenzyKeeper.frenzyState);
	}
}


function processOut(e) {
	//console.log("out")
	lineBeingDrawn = false;
	isTouchDown = false;

	board.drawGameCanvas("canvas2");
}

function drawFoundLine(x1,y1,x2,y2,color) {

	//To correct disaligned highlights.
	newPt = closestValidPoint(x1,y1,x2,y2);
	x2 = newPt.x;
	y2 = newPt.y;

	x1 = x1 * GV.tileWidth + 0.96*(GV.tileWidth/2);
	y1 = y1 * GV.tileWidth + 1.1*(GV.tileWidth/2);
	x2 = x2 * GV.tileWidth + 0.96*(GV.tileWidth/2);
	y2 = y2 * GV.tileWidth + 1.1*(GV.tileWidth/2);

	//get rgb part from color
	//color = color.substring(4);
	color = color.substring(4, color.length - 1);

	canvas = document.getElementById("canvas");

	cntxt = canvas.getContext("2d");
	cntxt.lineCap="round";
	//cntxt.strokeStyle = 'rgba(133, 222, 240, 0.4)';
	cntxt.strokeStyle = 'rgba(' + color + ', 0.12)';
	//cntxt.fillStyle = 'rgba(133, 222, 240, 0.4)';
	cntxt.fillStyle = 'rgba(' + color + ', 0.12)';
	cntxt.lineWidth = GV.tileWidth*9/10;

	cntxt.beginPath();
	cntxt.moveTo(x1,y1);
	cntxt.lineTo(x2,y2);

	cntxt.stroke();
	//cntxt.closePath();

}

function drawLine() {
	var x1 = Math.floor(startX/GV.tileWidth)*GV.tileWidth+GV.tileWidth/2;
	var y1 = Math.floor(startY/GV.tileWidth)*GV.tileWidth+GV.tileWidth/2;
	var x2 = Math.floor(endX/GV.tileWidth)*GV.tileWidth+GV.tileWidth/2;
	var y2 = Math.floor(endY/GV.tileWidth)*GV.tileWidth+GV.tileWidth/2;

	//Determine endpoints to make lines always diagonal.
	newPt = closestValidPoint(x1,y1,x2,y2);
	x2 = newPt.x;
	y2 = newPt.y;

	R = hexToR(GV.currentPlayerColor);
	G = hexToG(GV.currentPlayerColor);
	B = hexToB(GV.currentPlayerColor);
	//Canvas 2
	canvas2 = document.getElementById("canvas2");
	cntxt2 = canvas2.getContext("2d");
	cntxt2.lineCap="round";
	cntxt2.strokeStyle = 'rgba('+R+','+G+','+B+',0.2)';
	cntxt2.fillStyle = 'rgba('+R+','+G+','+B+',0.2)';
	cntxt2.lineWidth = GV.tileWidth*3/4.0;
	cntxt2.beginPath();
	cntxt2.moveTo(x1,y1);
	cntxt2.lineTo(x2,y2);
	cntxt2.stroke();
}

function drawTempLine() {
	var x1 = Math.floor(startX/GV.tileWidth)*GV.tileWidth+GV.tileWidth/2;
	var y1 = Math.floor(startY/GV.tileWidth)*GV.tileWidth+GV.tileWidth/2;
	var x2 = Math.floor(endX/GV.tileWidth)*GV.tileWidth+GV.tileWidth/2;
	var y2 = Math.floor(endY/GV.tileWidth)*GV.tileWidth+GV.tileWidth/2;

	//Determine endpoints to make lines always diagonal.
	/*
	newPt = closestValidPoint(x1,y1,x2,y2);
	x2 = newPt.x;
	y2 = newPt.y;
	*/

	R = hexToR(GV.currentPlayerColor);
	G = hexToG(GV.currentPlayerColor);
	B = hexToB(GV.currentPlayerColor);
	//Canvas 2
	canvas2 = document.getElementById("canvas");
	cntxt2 = canvas2.getContext("2d");
	if(
	  (x1 < 0 || x1 > canvas2.offsetWidth || y1 < 0 || y1 > canvas2.offsetHeight)
	  ||
	  (x2 < 0 || x2 > canvas2.offsetWidth || y2 < 0 || y2 > canvas2.offsetHeight)
	  )
		return;

	cntxt2.lineCap="round";

	//Line
	cntxt2.strokeStyle = 'rgba('+R+','+G+','+B+',0.05)';
	//cntxt2.strokeStyle = 'rgba(183, 217, 222, 0.1)';
	cntxt2.shadowColor = 'rgba(0, 0, 0, 1)';
	cntxt2.shadowOffsetX = -1;
	cntxt2.shadowOffsetY = -1;


	//Pattern
	/*
	var img = document.getElementById("pattern")
	var pat = cntxt2.createPattern(img,'repeat');
	cntxt2.strokeStyle = pat;
	cntxt2.globalAlpha = 0.4;
	*/

	cntxt2.lineWidth = GV.tileWidth*3/4.0;
	cntxt2.beginPath();
	cntxt2.moveTo(x1,y1);
	cntxt2.lineTo(x2,y2);
	cntxt2.stroke();
}

function closestValidPoint(x1, y1, x2, y2) {
	//Dimensions
	var xDim = Math.abs(x1-x2);
	var yDim = Math.abs(y1-y2);
	var maxDim = Math.max(xDim, yDim);
	var newPt = {x:0, y:0};

	//Valid Points
	var angleDeg = Math.atan2(y2 - y1, x2 - x1) * 180 / Math.PI;
	if(angleDeg < 0) angleDeg = 360 + angleDeg;
	if((angleDeg <= 20 && angleDeg >= 0) || (angleDeg <= 360 && angleDeg > 340)){
		//horizontal line
		newPt = {x:x1+maxDim, y:y1};
	}
	else if(angleDeg <= 70 && angleDeg > 20){
		//45 x=-y
		newPt = {x:x1+maxDim, y:y1+maxDim};
	}
	else if(angleDeg <= 110 && angleDeg > 70){
		//vertical
		newPt = {x:x1, y:y1+maxDim};
	}
	else if(angleDeg <= 160 && angleDeg > 110){
		//45 x=y
		newPt = {x:x1-maxDim, y:y1+maxDim};
	}
	else if(angleDeg <= 200 && angleDeg > 160){
		//horizontal
		newPt = {x:x1-maxDim, y:y1};
	}
	else if(angleDeg <= 250 && angleDeg > 200){
		//45 x=-y
		newPt = {x:x1-maxDim, y:y1-maxDim};
	}
	else if(angleDeg <= 290 && angleDeg > 250){
		//vertical
		newPt = {x:x1, y:y1-maxDim};
	}
	else if(angleDeg <= 340 && angleDeg > 290){
		//45 x=y
		newPt = {x:x1+maxDim, y:y1-maxDim};
	}

	return newPt;
}

function userVoiceIntegration(){
	// Include the UserVoice JavaScript SDK (only needed once on a page)
	UserVoice=window.UserVoice||[];(function(){var uv=document.createElement('script');uv.type='text/javascript';uv.async=true;uv.src='//widget.uservoice.com/'+GC.USER_VOICE_KEY+'.js';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(uv,s)})();

	//
	// UserVoice Javascript SDK developer documentation:
	// https://www.uservoice.com/o/javascript-sdk
	//

	// Set colors
	UserVoice.push(['set', {
		accent_color: '#448dd6',
		trigger_color: 'white',
		trigger_background_color: 'rgba(46, 49, 51, 0.6)',
		contact_title: 'Contact Us.',
		post_idea_title: 'Got and Idea?',
		contact_enabled: true,
		screenshot_enabled: false,
		smartvote_enabled: false,
		post_idea_enabled: true
	}]);

	// Identify the user and pass traits
	// To enable, replace sample data with actual user traits and uncomment the line
	UserVoice.push(['identify', {
		//email:		'john.doe@example.com', // User’s email address
		//name:		 'John Doe', // User’s real name
		//created_at: 1364406966, // Unix timestamp for the date the user signed up
		//id:		 123, // Optional: Unique id of the user (if set, this should not change)
		//type:		 'Owner', // Optional: segment your users by type
		//account: {
		//id:			 123, // Optional: associate multiple users with a single account
		//name:		 'Acme, Co.', // Account name
		//created_at:	 1364406966, // Unix timestamp for the date the account was created
		//monthly_rate: 9.99, // Decimal; monthly rate of the account
		//ltv:			1495.00, // Decimal; lifetime value of the account
		//plan:		 'Enhanced' // Plan name for the account
		//}
	}]);

	// Add default trigger to the bottom-right corner of the window:
	UserVoice.push(['addTrigger', { mode: 'satisfaction', trigger_position: 'bottom-right' }]);

	// Or, use your own custom trigger:
	//UserVoice.push(['addTrigger', '#id', { mode: 'satisfaction' }]);

	// Autoprompt for Satisfaction and SmartVote (only displayed under certain conditions)
	UserVoice.push(['autoprompt', {}]);
}

