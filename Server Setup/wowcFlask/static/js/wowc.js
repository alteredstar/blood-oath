
var Error = {
	EXPIRED: 100
}

var Dictionary = {
	SAT: "SAT",
	GEO: "GEOGRAPHY"
}



var colors = ["000fff", "00efff", "ff00ff", "00f00f", "ff9000", "ff143c"];
var playersInRow = 4;

var endGameClicked = false;
var applyFrenzyStyle = '';
maxNameLength = 12;
var maxWords = 8;

GV.gameState = GC.GameState.SOCKET_DISCONNECTED;
var gameKey = '';
var game;
var nick = '';
var isNickSet = false;
var profilePic = '';
var maxPlayerCount = 10;
GV.totalWords = 0;
var boardSize = 12;
var joinedPlayerCount = 1;
var isCreator = true;
var prevFoundWordCount = 0;
var found_word_coordinates = {};
var wordsFoundCount = 0;
var maxScore = 0;
var pulse = false;
var sortedPlayers = [];
var allPlayer = [];
//var slapSound = document.getElementById('slapSound');
var prevTurn = "";
var frenzyCount = 0;
var frenzyPlayer = "";
var rematch = false;
var postToWall = false;
var dictionary = "SAT";
var isSinglePlayer = true;
var invitedPlayersMap = new Object();
var joinedPlayerList = [];
//var cntxt;

$(document).ready(function() {
	init();
});

// initialise variables if current page is a link to an existing game
function init_joiner()
{
	gameKey = $.QueryString["gameKey"];
	isCreator = false;
	$("#btnStart").text("Join");
	$("#gameParams").hide();
}


// initialize all variables and hook all events
function init() {
	//initializeFacebook();

	if($.QueryString["gameKey"]) {
		init_joiner();
		$("#startScreen").hide();
		//$("#pnlConnect").show();
	}
	
	setNick();
	
	//var host =	location.origin.replace(/^http/, 'ws')
	//GV.webSocket = new WebSocket(host +":8888/socket");
	GV.webSocket = new SockJS("https://" + location.hostname + "/socket");
		console.log(location.hostname);
	//GV.webSocket = new WebSocket("ws://" + "54.191.177.22" + ":8888/socket");
	//updateNick();
	 
	// Websocket callbacks:
	GV.webSocket.onopen = function() {
		GV.gameState = GC.GameState.SOCKET_CONNECTED;
		console.log("Connected...");
		
		//Take joiner directly to wait screen on connecting - auto click join button.
		if($("#btnStart").text()==="Join")
			$("#btnStart").click();
	};
	GV.webSocket.onmessage = function(event) {
		console.log(event.data);
		data = JSON.parse(event.data);
		processMessage(data);
	};
	GV.webSocket.onclose = function() {
		console.log("Closed!");
		if(GV.gameState != GC.GameState.SOCKET_DISCONNECTED) {
			//alert("This game has expired.");
			show_message(jQuery.validator.format("This game has expired."));
			//window.location = window.location.pathname;
		}
	};
	GV.webSocket.onerror = function(error) {
		console.log('Error: ' + error);
	};

	// other callback

	if($.cookie("postToWall"))
		$('#postToWall').prop("checked",$.cookie("postToWall"));//previous selection from cookie
	else
		$('#postToWall').prop("checked",false);//default un-checked

	if($.cookie("wowColor"))
		$('#color1').val($.cookie("wowColor"));//previous selection from cookie
	else
		$('#color1').val("0000ff");//default blue
	$('#color1').colorPicker({showHexField: false,	colors: colors});
	
	if($.cookie("wowMaxPlr"))
		$('#maxPlr').val($.cookie("wowMaxPlr"));//previous selection from cookie
	else
		$('#maxPlr').val(maxPlayerCount);//default blue
	
	if($.cookie("wowBoardSz"))
		$('#boardSz').val($.cookie("wowBoardSz"));//previous selection from cookie
	else
		$('#boardSz').val(boardSize);//default blue
	
	//$("#btnRematch").click(rematchClick());
	
	$("#btnNewNick").click(function(){
		updateNick();
	});

	$("#btnStart").click(function(){
		console.log("START BUTTON IS AUTO-CLICKED");
		maxPlayerCount = $("#maxPlr").val();
		boardSize = $("#boardSz").val();
		//GV.currentPlayerColor = $("#plrClr").val();
		GV.currentPlayerColor = $("#color1").val();
		postToWall = $("#postToWall").prop("checked");

		$.cookie("wowMaxPlr",maxPlayerCount,{expires:1});//set max allowed players in a game
		$.cookie("wowBoardSz",boardSize,{expires:1});//set game board size
		$.cookie("wowColor",GV.currentPlayerColor,{expires:1});//set player color in cookie
		$.cookie("postToWall",postToWall,{expires:1});

		var style = document.createElement('style');
		style.type = 'text/css';
		//style.innerHTML = '.color' + $("#nick").val() + '{ color: ' + GV.currentPlayerColor + '; }' +
		//					'\n	.colorBG' + $("#nick").val() + ' { border-left: 5px solid yellow; } ';
		style.innerHTML = '.color' + GV.playerCache.facebook.profile.id + '{ color: ' + GV.currentPlayerColor + '; }' +
							'\n	.colorBG' + GV.playerCache.facebook.profile.id + ' { border-left: 5px solid yellow; } ';
		document.getElementsByTagName('head')[0].appendChild(style);

		connect();
	});

	/*
	$("#gameURL").click(function(){
	this.select();
	});
	*/

	$("#btnForceStart").click(function(){
		force_start();
	});
	

	//Google Analytics
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-53948652-1', 'auto');
	ga('send', 'pageview');

	window.onunload=endGame;
	window.onbeforeunload=goodbye;
	
	console.log("INIT DONE")
}

function goodbye(e) {
//	if ((GV.gameState==GC.GameState.WAITING_FOR_START || GV.gameState==GC.GameState.PLAYING) && endGameClicked) {
	if (GV.gameState==GC.GameState.WAITING_FOR_START || GV.gameState==GC.GameState.PLAYING) {
		// If we haven't been passed the event get the window.event
		e = e || window.event;

		var message = 'All game progress will be lost.';

		// For IE6-8 and Firefox prior to version 4
		if (e) 
		{
			e.returnValue = message;
		}

		// For Chrome, Safari, IE8+ and Opera 12+
		return message;
	}
//	else if(GV.gameState != GC.GameState.SOCKET_DISCONNECTED) {
//		//alert("Connection Lost!");
//		// Redirect if server connection lost
//	}
	else
		return null;
}
	

function updateJoinedPlayers() {
	var count = 0;
	var imgSize = 40;
	invitedPlayerTblInnerHTML = '<tr>'
	for (var i in invitedPlayersMap) {
		count++;
		tFbUsrId = i;
		
		if(tFbUsrId == GV.playerCache.facebook.profile.id) {
			invitedPlayerTblInnerHTML += '<td class="waitGridTd">'+
			'<div style="position:relative;"><img src="' + invitedPlayersMap[i].picture + '" height="'+imgSize+'" width="'+imgSize+'" class="waitPicImg joinedImg">' +
			'<img src="../static/images/tick.png" class="overlayImg" height="'+imgSize+'" width="'+imgSize+'" style="position:absolute; bottom:0px; right:0px;"></div>' +
			'<span class="waitGridSpan">' + invitedPlayersMap[i].first_name + '</span>' + 
			'</td>';
		}
		else if($.inArray(tFbUsrId, joinedPlayerList) >= 0) {
			console.log("Joined: " + invitedPlayersMap[i].first_name);
			invitedPlayerTblInnerHTML += '<td class="waitGridTd">'+
			'<div style="position:relative;"><img src="' + invitedPlayersMap[i].picture + '" height="'+imgSize+'" width="'+imgSize+'" class="waitPicImg joinedImg">' +
			'<img src="../static/images/tick.png" class="overlayImg" height="'+imgSize+'" width="'+imgSize+'" style="position:absolute; bottom:0px; right:0px;"></div>' +
			'<span class="waitGridSpan">' + invitedPlayersMap[i].first_name + '</span>' + 
			'</td>';
		}
		else {
			console.log("Waiting: " + invitedPlayersMap[i].first_name);
			invitedPlayerTblInnerHTML += '<td class="waitGridTd">'+
			'<div><img src="' + invitedPlayersMap[i].picture + '" height="'+imgSize+'" width="'+imgSize+'" class="waitPicImg waitingImg"></div>' +
			'<span class="waitGridSpan">' + invitedPlayersMap[i].first_name + '</span>' + 
			'</td>';
		}
		
		if(count % playersInRow == 0)
			invitedPlayerTblInnerHTML += '</tr><tr>'
	}
	invitedPlayerTblInnerHTML += '</tr>'
	
	$("#invitedFriendsTbl").html(invitedPlayerTblInnerHTML);
}

function rematchClick(){
	initUI();
	rematch = true;
	GV.gameState = GC.GameState.SOCKET_CONNECTED;
	console.log("Connected...for Rematch...");
	connect();
	}

function wallPost() {
	FB.ui({
		method: 'feed',
		name:'Won a WAR of WORDS..!',
		caption:'I just won a War of Words.',
		description:'Challenge your friends.',
		link: 'https://apps.facebook.com/gc_war_of_words'
	}, function(response) { 
	
	});
}

function endGame() {
	console.log("-- End Game --");
	GV.webSocket.send(JSON.stringify((jQuery.validator.format(GC.COMMANDS.end_game,nick,GV.playerCache.facebook.profile.id))));
	//$("#endGame").delay(2000);
	//window.location = window.location.pathname;
	//location.reload();
}

function gameOverCmd() {
	console.log('send game over cmd');
	GV.webSocket.send(JSON.stringify((jQuery.validator.format(GC.COMMANDS.game_over,nick,GV.playerCache.facebook.profile.id))));
	//window.location = window.location.pathname;
	//location.reload();
}

function sendFeedback() {
	rating = $('#rating').rateit('value');
	comments = $('#feedbackComments').val();
	GV.webSocket.send(JSON.stringify((jQuery.validator.format(GC.COMMANDS.feedback,GV.userKey,rating,comments))));
}

function setNick() {
	$("#nick").val(getRandomNick());
}

function updateNick() {
	$("#nick").val(getRandomNick());
}

function connect() {
	if (GV.gameState!=GC.GameState.SOCKET_CONNECTED) {
		invalidGamestateErrorRedirect(GV.gameState);
		return false;
	}
	console.log('IN CONNECT');
	var checkNick = function(){
		if(isNickSet){
			console.log("NICK IS SET");
			nick = $("#nick").val();
			if(rematch)
				GV.webSocket.send(JSON.stringify((jQuery.validator.format(GC.COMMANDS.rematch,nick,GV.playerCache.facebook.profile.id))));
			else
				GV.webSocket.send(JSON.stringify((jQuery.validator.format(GC.COMMANDS.connect,nick,GV.playerCache.facebook.profile.id,profilePic))));
			GV.gameState = GC.GameState.CONNECT_SENT;
		}
		else{
			$("#reloadDiv").show();
			setTimeout(checkNick, 1000);
			$("#reloadDiv").hide();
		}
	}
	checkNick();
	return false;
}

function start_game() {
	if (GV.gameState!=GC.GameState.USER_KEY_RECEIVED) {
		invalidGamestateErrorRedirect(GV.gameState);
		return false;
	}
	//maxPlayerCount = $("#numOfUsers").val();
	GV.webSocket.send(JSON.stringify((jQuery.validator.format(GC.COMMANDS.start_game,GV.userKey,maxPlayerCount,boardSize,rematch,maxWords,dictionary))));
	GV.gameState = GC.GameState.START_GAME_SENT;
	return false;
}

function join_game() {
	if (GV.gameState!=GC.GameState.USER_KEY_RECEIVED) {
		invalidGamestateErrorRedirect(GV.gameState);
		return false;
	}
	$("#pnlConnect").hide();
	gameJoinMessage();
	GV.webSocket.send(JSON.stringify((jQuery.validator.format(GC.COMMANDS.join_game,GV.userKey,gameKey))));
	GV.gameState = GC.GameState.WAITING_FOR_START;
	return false;
}

function force_start() {
	if (GV.gameState!=GC.GameState.WAITING_FOR_START) {
		invalidGamestateErrorRedirect(GV.gameState);
		return false;
	}
	GV.webSocket.send(JSON.stringify((jQuery.validator.format(GC.COMMANDS.force_start,GV.userKey))));
	GV.gameState = GC.GameState.WAITING_FOR_START;
	return false;
}

/*
function playSound() {
	slapSound.load();
	slapSound.play();
}
*/

function invalidGamestateErrorRedirect(gamestate) {
	console.log("Invalid Game State:" + gamestate);
	alert("Connection Lost!");
	window.location = window.location.pathname;
}

function display_game() {
	$("body").removeClass('body_bg_img');
	$("body").addClass('body_bg');

	$("#pnlConnect").hide();
	$("#gameBoard").show();
	$("#timer").show();
	$("#endGame").show();
	draw_game_grid();
	
	//showProgressBar();
	$("#progressBar").show();
	
}

function showProgressBar(){
	$("#progressBar").show();
	wdth = $("#canvas2").attr('width') + "px";
	lft = $("#canvas2").css("left");
	tp = $("#canvas2").css("top");
	tp = tp.substring(0, tp.length-2);
	tp = tp - 22;
	tp = tp + "px";
	
	$("#progressBar").css("max-width", wdth);
	$("progressBar").css("margin", "auto");
	$("#progressBar").css("position", "absolute");
	$("#progressBar").css("left", lft);
	$("#progressBar").css("top", tp);
}

function draw_game_grid() {
	FrenzyKeeper.FrenzyIsOn = false;
	var winWidth = $( window ).width();
	var canvasLeftPos = Math.floor((winWidth - game.game.length * tileWidth)/2);
	
	canvas = document.getElementById("canvas");
	var ctx = canvas.getContext("2d");
	$("#canvas").attr('width', game.game.length * tileWidth);
	$("#canvas").attr('height',game.game.length * tileWidth);
	ctx.clearRect(0, 0, canvas.width, canvas.height);
	ctx.font = ""+fontSize+"px " + canvasFont;
	//Overlay canvas
	canvas2 = document.getElementById("canvas2");
	var ctx2 = canvas2.getContext("2d");
	$("#canvas2").attr('width', game.game.length * tileWidth);
	$("#canvas2").attr('height',game.game.length * tileWidth);
	ctx2.clearRect(0, 0, canvas2.width, canvas2.height);

	
	rect = canvas2.getBoundingClientRect();
	$("#canvas").css("left", rect.left);
	$("#canvas").css("top", rect.top);
	
	$("#frenzyDiv").css("width", $("#canvas2").css("width"));
	var i = 0;
	for (var row in game.game)	{
		for (var col in game.game[row]) {
			//fill letters
			col = parseInt(col), row = parseInt(row)

			/*
			//FONT TESTING
			if(row == 1)
			ctx.font = ""+fontSize+"px NCSB";
			else if(row == 2)
			ctx.font = ""+fontSize+"px NCS";
			else if(row == 3)
			ctx.font = ""+fontSize+"px Ubuntu";
			else if(row == 4)
			ctx.font = ""+fontSize+"px Normt";
			else
			ctx.font = ""+fontSize+"px " + canvasFont;
			*/


			ctx.fillStyle = '#3b3b3b';
			ctx.fillStyle = '#888';
			ctx.fillText(game.game[col][row].toUpperCase(), (col)*tileWidth + tileWidth/4, (row+1)*tileWidth-tileWidth/4);

			//checkerboard pattern
			//TODO: make the pattern on canvas 1, before entering text.
			/*
			if((row % 2 == 0 && col % 2 == 0) || (row % 2 != 0 && col % 2 != 0)){
				ctx2.fillStyle = 'rgba(230, 230, 230, 0.24)';
				ctx2.fillRect(col * tileWidth, row * tileWidth, tileWidth, tileWidth);
			}
			*/
		}
	}

	//print wordlist and players
	var found_words = {};
	//var playerlist = '<h1 class="heading heading-blue">Score</h1><ol>'
	var playerlist = '<ol>'
	var playerCount = 0;
	var foundWordCount = 0;	
	var topPlayerColor = '';
	var currPlayerColor = '';
	//for (player in game.players) {
	//Player list + found words
	for (playerIndex in sortedPlayers) {
		player = sortedPlayers[playerIndex].name;
		playerName = game.players[player].nick;
		playerCount = sortedPlayers[playerIndex].num;

		if(player == GV.playerCache.facebook.profile.id)
			playerCount = 'me';
		else
			playerCount = 'Other';//all other players are grey - CSS:colorOther

			//Frenzy mode
		applyFrenzyStyle = '';

		if(game.players[player].is_frenzy){
			applyFrenzyStyle = ' frenzy ';
			if(player == GV.playerCache.facebook.profile.id)
				FrenzyKeeper.FrenzyIsOn = true;
			else
				FrenzyKeeper.FrenzyIsOn = false;
		}
		
		playerlist+= '<li id="li' + player + '" class="colorBG' + playerCount + applyFrenzyStyle + '">'+
						//'<div class="colorBG' + playerCount + ' playerBar">&nbsp;</div>' +
						'<img src="' + game.players[player].profile_pic + '" alt="' + playerName + '" class="profilePic">' +
						//'<span class="color'+playerCount + applyFrenzyStyle + (player==nick ? " me":"")+'">'+ player + '</span>' +
						//'<span class="color'+playerCount+applyFrenzyStyle+'">'+game.players[player].score + '</span>' +
						//'<span class="'+ applyFrenzyStyle + (player==nick ? " me":"")+'">'+ player + '</span>' +
						'<span class="playerName' + (player==GV.playerCache.facebook.profile.id ? " me":"")+'">'+ (player==GV.playerCache.facebook.profile.id ? "Me" : playerName) + '</span>' +
						'<span class="score">'+game.players[player].score + '</span>' +
						'<span style="display: none;" class="star color'+playerCount+' glyphicon glyphicon-star"></span>' +
					 '</li>';
		
		if(player == GV.playerCache.facebook.profile.id)
			currPlayerColor = $('.color'+playerCount).css('color');
		
		if(game.players[player].score >= maxScore) {
			maxScore = game.players[player].score;
			topPlayerColor = $('.color'+playerCount).css('color');
		}

		for(word in game.players[player].found_words) {
			word = game.players[player].found_words[word]["word"][0];
			found_words[word]=playerCount;
			var playerClass = 'color'+playerCount;
			var playerColor = $("."+playerClass).css('color');
			
			//TODO: if unable to find color from css using jquery, traverse stylesheets - need to find a better/optimal way.
			if(!playerColor) {
				playerColor = getCSS('color', playerClass);
			}
			
			var tmp_found_word_coordinates = {};
			tmp_found_word_coordinates[1] = game.players[player].found_words[word]["x1"];
			tmp_found_word_coordinates[2] = game.players[player].found_words[word]["y1"];
			tmp_found_word_coordinates[3] = game.players[player].found_words[word]["x2"];
			tmp_found_word_coordinates[4] = game.players[player].found_words[word]["y2"];
			tmp_found_word_coordinates[5] = playerColor;

			found_word_coordinates[foundWordCount] = tmp_found_word_coordinates;

			foundWordCount++;
		}	
		playerCount++;
	}
	playerlist += '</ol>'
	$("#playerlist").html(playerlist);
	 
	//Wordlist + wordcount
	//TODO: Wordlist heading removed.
	//var wordlist = '<h1 class="heading heading-blue">WORDS</h1><ul>';
	var wordlist = '<ul>';
	for(w in game.displayed_words) {
	word = game.displayed_words[w];
	if(word == game.hot_words[word] && !found_words.hasOwnProperty(word))
		//wordlist += '<li'+(found_words.hasOwnProperty(word)? ' class="word foundWord color'+ found_words[word]+'"' :' class="word hotword"')+'>' + word +'</li>'
		wordlist += '<li class="word hotword">' + word.toUpperCase() +'<span class="pts2">2x</span></li>'
	else
		wordlist += '<li'+(found_words.hasOwnProperty(word)? ' class="word foundWord s"' :' class="word"')+'>' + word.toUpperCase() +'</li>'
	}
	wordlist +='</ul>'
	
	var wordCount = '';
	if(GV.totalWords > 0)
		wordCount = '<span class="wordCount">' + wordsFoundCount + '/' + GV.totalWords + '</span>';
	
	//TODO: Wordcount removed from wordlist
	//wordlist += wordCount;
	$("#wordlist").html(wordlist);
	
	
	//Canvas
	//updateCanvasBorder(topPlayerColor, currPlayerColor);
	for(word in found_word_coordinates){
		drawFoundLine(found_word_coordinates[word][1], found_word_coordinates[word][2], found_word_coordinates[word][3], found_word_coordinates[word][4], found_word_coordinates[word][5]);
	}
}

function getStyleRuleValue(style, selector, sheet) {
	var sheets = typeof sheet !== 'undefined' ? [sheet] : document.styleSheets;
	for (var i = 0, l = sheets.length; i < l; i++) {
		var sheet = sheets[i];
		if( !sheet.cssRules ) { continue; }
		for (var j = 0, k = sheet.cssRules.length; j < k; j++) {
			var rule = sheet.cssRules[j];
			if (rule.selectorText && rule.selectorText.split(',').indexOf(selector) !== -1) {
				return rule.style[style];
			}
		}
	}
	return null;
}

function getCSS(prop, fromClass) {
	var $inspector = $("<div>").css('display', 'none').addClass(fromClass);
	$("body").append($inspector); // add to DOM, in order to read the CSS property
	try {
		return $inspector.css(prop);
	} finally {
		$inspector.remove(); // and remove from DOM
	}
}

function updateCanvasBorder(topPlayerColor, currPlayerColor) {
	//Outline is colored as current player color
	//$("#canvas2").css('outline', 'medium double ' + currPlayerColor);
	//$("#canvas").css('outline', 'medium double ' + currPlayerColor);
	
	//Border is colored as current leader color.
	//to flicker the border when word found.
	if(pulse) {
		$("#canvas2").css('border', 'medium solid ' + topPlayerColor);
		$("#canvas2").css('border-radius', '4px');
		$("#canvas").css('border', 'medium solid ' + topPlayerColor);
		$("#canvas").css('border-radius', '4px');
	
		var flashCount = 0;
		setInterval( function(){
			if(flashCount>10) return;
			flashCount++;
			if(flashCount % 2) {
				$("#canvas2").css('border', 'medium solid ' + topPlayerColor);
				$("#canvas").css('border', 'medium solid ' + topPlayerColor);
			}
			else {
				$("#canvas2").css('border', 'medium solid white');
				$("#canvas").css('border', 'medium solid white');
			}
		}, 50);
		
		$("#canvas2").css('border', 'medium solid ' + topPlayerColor);
		$("#canvas").css('border', 'medium solid ' + topPlayerColor);
	}
	$("#canvas2").css('border', 'medium solid white');
	$("#canvas").css('border', 'medium solid white');
}

/*
function drawLine(x1,y1,x2,y2) {
	canvas = document.getElementById("canvas");
	var ctx = canvas.getContext("2d"); 
	ctx.beginPath();
	ctx.moveTo(Math.floor(startX/tileWidth)*tileWidth+tileWidth/2, Math.floor(startY/tileWidth)*tileWidth+tileWidth/2);
	ctx.lineTo(Math.floor(endX/tileWidth)*tileWidth+tileWidth/2, Math.floor(endY/tileWidth)*tileWidth+tileWidth/2);
	ctx.lineCap="round";
	ctx.strokeStyle = 'rgba(255,255,0,0.4)';
	ctx.fillStyle = 'rgba(255,255,0,0.4)';
	ctx.lineWidth = tileWidth*3/4.0;
	ctx.stroke();
	ctx.closePath();
	
}
*/


function game_over() {
	//if(isCreator)
		//$("#rematch").show();
	var max = -1;
	for (player in game.players) {
		if (game.players[player].score > max) {
			max = game.players[player].score;
		}
	}
	var isWinner = false;
	var winners = new Array();
	for (player in game.players) {
		if (max == game.players[player].score) {
			if(player == GV.playerCache.facebook.profile.id) {
				isWinner = true;
			}
			winners.push(game.players[player].nick);
		}
	}
	
	if(isWinner)
	{
		$("#wallPost").show();
		if(postToWall) {
			wallPost();
		}
	}

	show_message ("Game won by "+ winners);
	draw_game_grid();
	return winners;
}

function gameOverCanvas(winners){
	GV.gameState = GC.GameState.GAME_OVER;
	unbindTouchHandlers();
	$("#progressBar").hide();
	
	canvas = document.getElementById("canvas");
	var ctx = canvas.getContext("2d");	
	$("#canvas").css('opacity' ,'0.1');
	
	canvas2 = document.getElementById("canvas2");
	var ctx2 = canvas2.getContext("2d");	
	$("#canvas2").attr('width', game.game.length * tileWidth);
	$("#canvas2").attr('height',game.game.length * tileWidth);
	ctx2.clearRect(0, 0, canvas.width, canvas.height);
	
	ctx2.font = ""+fontSize+"px " + canvasFont;
	ctx2.textAlign="center"; 
	ctx2.fillText("GAME OVER",canvas.width/2,canvas.height/2); 
	ctx2.font = "14px Trebuchet MS";
	ctx2.fillText("Game won by "+winners,canvas.width/2,(3*canvas.height)/4); 
	
	rect = canvas2.getBoundingClientRect();
	$("#canvas").css("left", rect.left);
	$("#canvas").css("top", rect.top);
	
	$("#frenzyDiv").css("width", $("#canvas2").css("width"));
	/*
	timerWidth = $("#timer").css("width");
	timerWidth = timerWidth.substring(0, timerWidth.length - 2);
	if(timerWidth < 70){
		$("#timer").css("width", "70px");
		$("#timer").css("width", "auto");
	}
	*/
}

function display_game_url() {
	$("#pnlConnect").hide();
	$("#pnlStart").show();
	$("#invitedFriendsTbl").show();
	$("#invite").show();
	gameJoinMessage();
}

function processMessage(message) {
	//debugger;
	// connected and user key received
	if (GV.gameState==GC.GameState.CONNECT_SENT && message.key ) {
		GV.userKey = message.key;
		GV.gameState = GC.GameState.USER_KEY_RECEIVED;
		if(message.owner && message.owner == GV.playerCache.facebook.profile.id) {
			isCreator = true;
			rematch = true;
		}
		else {
			rematch = false;
		}
		if (isCreator) {
			if(message.gamekey){
				gameKey = message.gamekey;
				join_game();
			}
			else
				start_game();
			$("#endGame").show();
		}
		else {
			join_game();
			$("#endGame").hide();
		}
	}
	// waiting for start
	else if (GV.gameState==GC.GameState.START_GAME_SENT && message.game) {
		gameKey = message.game;
		GV.gameState = GC.GameState.WAITING_FOR_START;
		if(GV.isSinglePlayer) {
			force_start();
		} else {
			display_game_url();
		}
	}
	// receive game and render the board <<OS-Here>>
	else if (GV.gameState==GC.GameState.WAITING_FOR_START && message) {
		game = message.game;
		console.log("real log");
		console.log(game);
		if(game) {
			GV.gameState = GC.GameState.PLAYING;
			$("#pnlStart").hide();
			$("#invitedFriendsTbl").hide();
			$("#invite").hide();
			//show_message("");
			show_message("&nbsp;");
			wordsFoundCount = game.found_word_count;
			var tempTotalCount = game.displayed_word_count + game.left_word_count;
			if(GV.totalWords < tempTotalCount) {
				GV.totalWords = tempTotalCount;
			}
			pulse = false;
			if(game.ongoing) {
				//player joins an ongoing game
				game.players = sortPlayers(game.players);
			}
			else {
				sortPlayers(0);
			}
			display_game();
			display_game();
			//playSound();
		} else {
			var waiting = message;
			if(waiting.players_joined_count) {
				joinedPlayerCount = waiting.players_joined_count;
			}
			if(waiting.players_joined) {
				joinedPlayerList = waiting.players_joined;
			}
			var owner = waiting.owner;
			if(GV.playerCache.facebook.profile.id == owner) {
				gameJoinMessage();
			} else {
				gameJoinMessage();
			}
			$("#endGame").show();
		}
	}
	// process updates from server
	else if (GV.gameState==GC.GameState.PLAYING) {
		if (message.error) {
			// Error
			pulse = false;
			console.log(message.textStatus)
			display_game();
		} else if (message) {
			// GAME OVER!
			var updates = message;
			if (updates.game_over) {
				game.players = updates.game_over.players;
				GV.totalWords = 0;
				winners = game_over();
				FrenzyKeeper.updateTimer(-1);
				gameOverCanvas(winners);
				gameOverCmd();//Cleanup command to server
				//endGame();
			}
			// SUCCESSFUL MOVE
			else if (updates.displayed_words) {
				//Update word list
				pulse = true;
				game.displayed_words = updates.displayed_words;
				game.hot_words = updates.hot_words;
				game.players = sortPlayers(updates.players);
				wordsFoundCount = updates.found_word_count;
				var tempTotalCount = updates.displayed_word_count + updates.left_word_count;
				if(GV.totalWords < tempTotalCount) GV.totalWords = tempTotalCount;
				//frenzyUpdate(updates);
				display_game();
				if(updates.turn == GV.playerCache.facebook.profile.id) {
					//I found this word
					FrenzyKeeper.isWordFoundInTen(updates.frenzy_word_count);
				}


				//playSound();
			}
			//NEW PLAYER JOINS A GAME IN-PROGRESS
			else if(updates.game) {
				updates = updates.game;
				game.players = sortPlayers(updates.players);
				wordsFoundCount = updates.found_word_count;
				var tempTotalCount = updates.displayed_word_count + updates.left_word_count;
				if(GV.totalWords < tempTotalCount) GV.totalWords = tempTotalCount;
				//frenzyUpdate(updates);
				display_game();
				if(updates.turn == GV.playerCache.facebook.profile.id) {
					//I found this word
					FrenzyKeeper.isWordFoundInTen(updates.frenzy_word_count);
				}

				//playSound();
			}
			else if (updates.rematch) {
				gameKey = updates.rematch;
				window.location = window.location.origin + "/?gameKey="+gameKey;
				//get url, append game key, and redirect.
			}
			else {
				//Succesful move
				pulse = true;
				//game.players = updates.players;
				game.players = sortPlayers(updates.players);
				wordsFoundCount = updates.found_word_count;
				var tempTotalCount = updates.displayed_word_count + updates.left_word_count;
				if(GV.totalWords < tempTotalCount) GV.totalWords = tempTotalCount;
				//frenzyUpdate(updates);
				display_game();
				if(updates.turn == GV.playerCache.facebook.profile.id) {
					//I found this word
					FrenzyKeeper.isWordFoundInTen(updates.frenzy_word_count);
				}
				//playSound();
			}
		} else {
			console.log("Don't know what to do")
		}
	}
	else {
		if (message.error) {
			console.log(message.textStatus)
			if(message.errorCode == Error.EXPIRED) {
				show_message(jQuery.validator.format("This game has expired."));
				$("#endGame").show();
			}
		}
		else {
			console.log("Unhandled Message: " + message);
		}
	}
}

function frenzyUpdate(updates) {
	if(prevTurn == '' || prevTurn != updates.turn) {
		prevTurn = updates.turn;
		frenzyCount = 0;
	}
	else if(prevTurn == updates.turn) {
		frenzyCount++;
	}
	
	if(frenzyCount>=2) {
		frenzyPlayer = prevTurn;
		FrenzyKeeper.FrenzyIsOn = true;
	}
}


function gameJoinMessage() {
	
	//Diabling message - display pics shown.
	if(!isCreator) {
		count = joinedPlayerCount;
		if(count == 0)
			show_message(jQuery.validator.format(" Waiting for others to join."));
		else if(count == 1)
			show_message(jQuery.validator.format(" 1 player has joined."));
		else if(count > 1)
			show_message(jQuery.validator.format(" {0} players have joined.",count));
	}
	else
		show_message("&nbsp;");
	
	updateJoinedPlayers();
}

$(window).resize(function(){
	 if(GV.gameState==GC.GameState.PLAYING) display_game();
});

$(window).scroll(function(){
	 if(GV.gameState==GC.GameState.PLAYING) display_game();
});

