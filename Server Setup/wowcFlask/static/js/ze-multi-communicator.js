(function (ZeMultiCommunicator, $, undefined){
	ZeMultiCommunicator.connect = function(gameKey) {
		console.log(GV.playerCache.wow.player_id);
		GV.webSocket.send(JSON.stringify((jQuery.validator.format(GC.COMMANDS.connect, gameKey, GV.playerCache.wow.player_id, GV.playerCache.facebook.profile.id, GV.playerCache.facebook.profile.profilePic))));
	};

	ZeMultiCommunicator.join_game = function(gameKey) {
		if (GV.gameState!=GC.GameState.USER_KEY_RECEIVED) {
			invalidGamestateErrorRedirect(GV.gameState);
			return false;
		}
		GV.webSocket.send(JSON.stringify((jQuery.validator.format(GC.COMMANDS.join_game, GV.userKey, gameKey))));
		GV.gameState = GC.GameState.WAITING_FOR_START;
		return false;
	}

	ZeMultiCommunicator.force_start = function() {
		if (GV.gameState != GC.GameState.WAITING_FOR_START) {
			invalidGamestateErrorRedirect(GV.gameState);
			return false;
		}
		GV.webSocket.send(JSON.stringify((jQuery.validator.format(GC.COMMANDS.force_start, GV.userKey))));
		GV.gameState = GC.GameState.PLAYING;
		return false;
	}

	ZeMultiCommunicator.move = function(move, userKey, x1, y1, x2, y2, word, frenzyState) {
		if (GV.gameState != GC.GameState.PLAYING) {
			invalidGamestateErrorRedirect(GV.gameState);
			return false;
		}

		var is_found_in_displayed = false;
		var reverse_word = reverse(word);
		for(w in GV.gameCache.words[GV.displayedPageNumber]) {
			wordObj = GV.gameCache.words[GV.displayedPageNumber][w];
			if (wordObj.word == word || wordObj.hint == word) {
				is_found_in_displayed = true;
				break;
			} else if (wordObj.word == reverse_word || wordObj.hint == reverse_word) {
				is_found_in_displayed = true;
				break
			}
		}

		if (is_found_in_displayed) {
			var moveMessage = jQuery.validator.format(move, userKey, x1, y1, x2, y2, word, frenzyState);
			GV.webSocket.send(JSON.stringify(moveMessage));
		} else {
			playAudio(GC.WORD_INVALID_AUDIO);
			//redraw the board
			board.displayLevel();
		}

		return false;
	}

	ZeMultiCommunicator.setProfilePicture = function(facebook_id) {
		$('.fb'+facebook_id).attr('src', GV.multiplayerGamePlayers[facebook_id].profilePic);
	};

	ZeMultiCommunicator.buildMultiplayerWaitingList = function(message) {
		$('.waitingPlayersContainer').empty();
		var waitingPlayersContainer = $(".waitingPlayersContainer");

		var players = message.players;
		var html = "<div class='waitingPlayersWrapper verticalAlignMiddle'>";
		for (var id in players){
			var player = players[id];
			var playerHtml = "<div class='player'>";
			if (player.facebook_id != '') {
				if (!GV.multiplayerGamePlayers[player.facebook_id]) {
					GV.multiplayerGamePlayers[player.facebook_id] = {"profilePic":""}
				}
				var profilePic = GV.multiplayerGamePlayers[player.facebook_id].profilePic;
				if (profilePic && profilePic != '') {
					playerHtml += "<img src='" + profilePic + "' width='50' height='50' class='dp " + (!player.joined?'blur':'') + "'>";
				} else {
					playerHtml += "<img src='../../../static/images/multiplayer/icons/avatar.png' width='50' height='50' class='dp " + (!player.joined?'blur':'') + " fb" +player.facebook_id+ "'>";
					getFbInvitedUsersInfo([player.facebook_id]);
				}
			}
			if (player.joined) {
				playerHtml += "<img src='../../../static/images/multiplayer/icons/tick.png' width='15' class='tick'>";
			}
			playerHtml += "</div>";
			html += playerHtml;
		}
		html += "</div>";

		waitingPlayersContainer.html(html);
	};

	ZeMultiCommunicator.processMessage = function(message){
//		console.log("ZeMultiCommunicator.processMessage called!");
		console.log(message);

		if (message.error) {
			// Error
			board.displayLevel();
			return;
		}

		var refresh = false;
		var game_end = false;
		var turn = null;
		if (message.command && message.command == "connect") {
			GV.userKey = message.key; //users unique key
			GV.gameState = GC.GameState.USER_KEY_RECEIVED;
			ZeMultiCommunicator.join_game(GV.gameKey);
		} else if (message.command && message.command == "join_game") {
			//Once connected build the waiting players' list
			playAudio(GC.PLAYER_JOINED_AUDIO);
			ZeMultiCommunicator.buildMultiplayerWaitingList(message)
		} else if (message.command && message.command == "force_start") {
			GV.gameCache = message.game.game_data;
			ZeCommunicator.startMultiplayerGame(GV.gameKey);
		} else if (message.command && message.command == "new_page") {
			playAudio(GC.NEW_PAGE_AUDIO);
			GV.gameCache.displayed_page_number = message.displayed_page_number;
			GV.gameCache.words = message.words;
//			refresh = true;
			GV.loadNewPage = true;
		} else if (message.command && message.command == "move") {
			playAudio(GC.WORD_VALID_AUDIO);
            GV.gameCache.players = message.players;
            turn = message.turn;
            refresh = true;
		} else if (message.command && message.command == "game_over") {
			refresh = true;
			game_end = true;
		} else if (message.command && message.command == "refresh_players") {
			GV.gameCache.players = message.players;
			GV.displayedPageNumber = GV.gameCache.displayed_page_number;
			board.displayGamePlayers();
		}

		if (refresh) {
			if (turn != null && turn == GV.playerCache.wow.player_id) {
				FrenzyKeeper.isWordFoundInTen(GV.gameCache.players[GV.playerCache.wow.player_id].frenzy_word_count);
			}
			GV.displayedPageNumber = GV.gameCache.displayed_page_number;
			board.displayPlayerScore();
			board.displayGamePlayers();
			board.displayLevel();

			if (game_end) {
				setTimeout(function(){
					ZeCommunicator.endMultiplayerGame(GV.gameKey, GV.gameCache);
					GV.gameKey = null;
					GV.gameState = GC.GameState.NONE;
				}, 1000);
			}
		}
	};
}(window.ZeMultiCommunicator = window.ZeMultiCommunicator || {}, jQuery));
