function initializeFacebook() {
	console.log("initializeFacebook()");

	//INITIALIZE FB API
	FB.init({
		appId: GC.FB_APP_ID,
		status: false,
		cookie: true,
		xfbml: true
	});
//	FB.Event.subscribe('auth.authResponseChange', onAuthResponseChange);
//	FB.Event.subscribe('auth.statusChange', onStatusChange);
	FB.getLoginStatus(getLoginStatusCallback);
}

var callbackForPopupBlockingCheck = function(popup_window) {
	if (popup_window != undefined) {
		popup_window.close();
	}
	connectFacebook(popupBlockerChecker.getIsBlocked());
}

function getLoginStatusCallback(response) {
	console.log("getLoginStatusCallback(): status=" + response.status);

	if (GV.loadedInFB == true) { //Game is loaded in an FB Canvas
		if (response.status == "connected" && response.authResponse != null) {
			fillFBCache(response);
		} else {
			connectFacebook(false);
		}
	} else { //Game loaded outside of the Canvas
		if (response.status == "connected" && response.authResponse != null) {
			fillFBCache(response);
		} else {
			ZeCommunicator.showLanding(); //Ask the user to click a button to authorize the app.
		}
/*		if (response.status == "unknown") {
			console.log("getLoginStatusCallback: unknown");
			GV.playerCache.facebook.tries++;
			var popup = window.open("https://www.google.com", '', 'width=200, height=100');
			popupBlockerChecker.check(popup, callbackForPopupBlockingCheck);
		} else if (response.status == "not_authorized") {
			console.log("getLoginStatusCallback: not_authorized")
			var popup = window.open("https://www.google.com", '', 'width=200, height=100');
			popupBlockerChecker.check(popup, callbackForPopupBlockingCheck);
	//		ZeCommunicator.showLanding();
		} else if (response.status == "connected" && response.authResponse != null) {
			console.log("getLoginStatusCallback: connected")
			fillFBCache(response);
		}
*/
	}

}

function connectFacebook(isPopupBlocked) {
	console.log("connectFacebook()");
	if (isPopupBlocked) { //redirect to the FB Login page
		window.location = encodeURI("https://www.facebook.com/dialog/oauth?client_id=885514164811444&redirect_uri="+GC.FB_REDIRECT_URI+"&response_type=token&scope=user_friends,email");
	} else {
		FB.login(connectFacebookCallback, {scope: 'user_friends,email'});
	}
}

function connectFacebookCallback(response) {
	console.log("connectFacebookCallback()");

	if (response.status === 'connected') {
		console.log("connectFacebookCallback(): connected");
		fillFBCache(response);
	} else if(response.status === 'not_authorized') {
//		console.log("connectFacebookCallback(): " + response.status);
//			top.location.href = 'https://www.facebook.com/games/gc_war_of_words';
	} else{
		/*FB.login(function(response){
			//New user - accepting invite now - need to redirect to gamekey in request once he has accepted the invite.
			if(response.authResponse){
				data = {'oauth_token':response.authResponse.accessToken};
				$.post($("request").context.URL, data, function(response){
					try {
						data = JSON.parse(response);
						if(data.gamekey) {
							console.log("FOUND data.gamekey");
							//window.location = window.location.origin + "/?gameKey="+data.gamekey;
						}
					}
					catch(err) {
						console.log("catch(err)");
						console.log(err.message);
						//location.reload(true);
					}
				});
				console.log("FB AUTH");
			}
//			getMyFBInfo(getFBInfoCallback);
		});*/
	}
//	isNickSet = true;
}

function disconnectFacebook() {
	FB.logout(function(response) {
		// Person is now logged out
		hide('#btnFacebookLogout');
		show('#btnFacebookLogin');
		delete GV.playerCache.facebook;
	});
}

var callTheCaller = null;
function getLoginStatusCallbackForMisc(response) {
	console.log("getLoginStatusCallbackForMisc(): status=" + response.status);
	if (response.status == "unknown" || response.status == "not_authorized") {
		//initiate login flow
	} else if (response.status == "connected" && response.authResponse != null) {
		if (callTheCaller != null) {
			callTheCaller();
		}
	}
}

var savePlayerInvitations = function(checkLogin) {
	//let's reset this dude
	callTheCaller = null;
	if (checkLogin) {
		//we want to call ourself if the login is valid
		callTheCaller = savePlayerInvitations;
		//check if INVITING user is logged in
		FB.getLoginStatus(getLoginStatusCallbackForMisc);
		return;
	}
	//else
	FB.ui({
		method: 'apprequests',
		message: 'Come play War of Words! It is great game!',
		filters: ['app_non_users'],
		max_recipients: '10',
	}, function(response) {
		if (response != null && response.to && response.to.length > 0) {
			ZeCommunicator.savePlayerInvitations(response.to);
		} else {
			console.log('Unable to send invitations. Try again later.');
		}
	});
}

var getGameKeyCallback = function(gameKey) {
	if (gameKey != GC.GAME_KEY_UNKNOWN) {
		FB.ui({
			method: 'apprequests',
			message: 'Lets have a War of Words.',
//			filters: ['app_users'],
			max_recipients: '5',
			data: gameKey,
		}, function(response) {
			if (response != null && response.to && response.to.length > 0) {
				ZeCommunicator.handleMultiplayerGameRequest(gameKey, response.to);
			} else {
				console.log('Unable to send game requests. Try again later.');
			}
		});
	} else {
		alert('Unable to send request at this time. Try again later!');
	}
}

var sendGameRequests = function(checkLogin) {
	//let's reset this dude
	callTheCaller = null;
	if (checkLogin) {
		//we want to call ourself if the login is valid
		callTheCaller = sendGameRequests;
		//check if user is logged in
		FB.getLoginStatus(getLoginStatusCallbackForMisc);
		return;
	}
	//else
	ZeCommunicator.getMultiplayerGameKey(getGameKeyCallback);
}

function getFbInvitedUsersInfo(invitedPlayerIds) {
//	getMyFBInfo(getFbInvitedUsersInfoCallback);
	for (var i = 0; i < invitedPlayerIds.length; i++) {
		invitedPlayerId = invitedPlayerIds[i];
		getAnyFBUserInfo(invitedPlayerId, getFbInvitedUsersInfoCallback);
	}
}

function getFbInvitedUsersInfoCallback(response) {
	if(response) {
		fn = '';
		if(response.first_name) {
			fn = response.first_name;
		}
		pic = '';
		if(response.picture) {
			pic = response.picture.data.url;
		}
		console.log("ID: " + response.id + " **** FN: " + response.first_name);
		if(response.id) {
			GV.multiplayerGamePlayers[response.id] = {"firstName": fn, "profilePic": pic};
			ZeMultiCommunicator.setProfilePicture(response.id);
		}

//		gameJoinMessage();
	}
}

function getFBInfoCallback(response) {
	console.log("getFBInfoCallback()");
	var profile = {};
	var firstName = response.first_name;

	if(firstName.length > 12) {
		firstName = firstName.substring(0, maxNameLength);
	}
	$("#nick").val(firstName);//+response.last_name + '-' + response.id);
	profile.id = response.id;
	profile.firstName = firstName;
	profile.fullName = response.name;
	profile.profilePic = response.picture.data.url;
	$("#profilePic").attr("src",profile.profilePic);
	$("#profilePic").attr("alt",profile.fullName);
	GV.playerCache.facebook.profile = profile;
	isNickSet = true;
	console.log(GV.playerCache);


	ZeCommunicator.getOrCreatePlayer(GV.playerCache.facebook.accessToken);

	FB.api(
		"/me/friends",
		function (response) {
			if (response && !response.error) {
				console.log(response);
		}
	}
);

	if(GV.playerCache.facebook.profile != undefined && GV.playerCache.facebook.profile != null) {
		hide('#btnFacebookLogin');
		show('#btnFacebookLogout');
	}
}

function getAnyFBUserInfo(fbUserId, callback) {
	console.log("getAnyFBUserInfo()");
	FB.api('/'+fbUserId, {fields:'id, name, first_name, last_name, picture.width(120).height(120)'}, callback);
}

function getMyFBInfo(callback) {
	console.log("getMyFBInfo()");
	FB.api('/me', {fields:'id, name, first_name, last_name, picture.width(120).height(120)'}, callback);
}

function fillFBCache(response) {
	GV.playerCache.facebook.accessToken = response.authResponse.accessToken;
	getMyFBInfo(getFBInfoCallback);
}

function getPermissions(callback) {
	FB.api('/me/permissions', function(response){
		if( !response.error ) {
			GV.playerCache.facebook.permissions = response.data;
			callback();
		} else {
			console.error('/me/permissions', response);
		}
	});
}

function hasPermission(permission) {
	for( var i in GV.playerCache.facebook.permissions ) {
		if(GV.playerCache.facebook.permissions[i].permission == permission
		&& GV.playerCache.facebook.permissions[i].status == 'granted' ) {
			return true;
		}
	}
	return false;
}

function getFriends(callback) {
	FB.api('/me/friends', {fields:'id, name, first_name, last_name, picture.width(120).height(120)'}, function(response){
		if( !response.error ) {
			GV.playerCache.facebook.friends = response.data;
			callback();
		} else {
			console.error('/me/friends', response);
		}
	});
}



