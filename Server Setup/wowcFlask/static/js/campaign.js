/*this file will handle the client-side-server logic for a single player game. From validation to scoring*/

function checkMove(y1, x1, y2, x2, word, frenzyState) {
	if (!GV.gameCache) {
		alert("Oops! Something bad happened. Please reload level to play again!");
		return;
	} else {
		var reverse_word = reverse(word);
		var is_reverse = false;
		var is_found_in_displayed = false;
		var is_already_found = false;
		var board = GV.gameCache.game;

		//find the hint against the word, but why?
		var hint = ""
		for(w in GV.gameCache.words[GV.displayedPageNumber]) {
			wordObj = GV.gameCache.words[GV.displayedPageNumber][w];
			if (wordObj.word == word || wordObj.hint == word) {
				hint = wordObj.hint;
				is_found_in_displayed = true;
				break;
			} else if (wordObj.word == reverse_word || wordObj.hint == reverse_word) {
				hint = wordObj.hint;
				is_reverse = true;
				is_found_in_displayed = true;
				word = reverse_word;
				break
			}
		}

		//if the word was found in displayed words, then make sure it hasn't already been found
		if (is_found_in_displayed) {
			playAudio(GC.WORD_VALID_AUDIO);
			var hot_word_score = 1;
			//check if it also a hot word
			for(w in GV.gameCache.words[GV.displayedPageNumber]) {
				wordObj = GV.gameCache.words[GV.displayedPageNumber][w];
				if(wordObj.word === word && wordObj.is_hot) {
					hot_word_score = GV.gameCache.level.meta.hotword_score;
					break;
				}
			}

			var list_of_words_found_by_player = GV.gameCache.players[GV.playerCache.wow.player_id].found_words;
			for (index in list_of_words_found_by_player) {
				found_word_dict = list_of_words_found_by_player[index];
				if (found_word_dict.word[0] == word) {
					console.log("OHNO! Word already found!")
					is_already_found = true;
					break;
				}
			}
			if (!is_already_found) {
				//Update Game Cache
				// Twitch for now: This should probably be removed and replaced with a found flag in the words, makes it easy to switch page and eventually save game.
				GV.gameCache.found_word_count = (GV.gameCache.found_word_count|| 0) + 1;

				var foundWord = {}
				foundWord.word = []
				foundWord.word.push(word);
				foundWord.word.push(hint);
				foundWord.x1 = x1;
				foundWord.y1 = y1;
				foundWord.x2 = x2;
				foundWord.y2 = y2;
				foundWord.found_time = new Date();

				GV.gameCache.players[GV.playerCache.wow.player_id].found_words[word] = foundWord;

				if (GV.gameCache.level.meta.frenzy_allowed) {
					//update frenzy status
					FrenzyKeeper.isWordFoundInTen(null);
				}

				var scoreIncrement = 0;
//				console.log("SCORE BEFORE: "+GV.gameCache.players[GV.playerCache.wow.player_id].score);
				if (FrenzyKeeper.frenzyState == FrenzyKeeper.State.ACTIVE_FRENZY) {
					scoreIncrement = (GV.gameCache.level.meta.word_score + (GV.gameCache.level.meta.frenzy_factor * GV.gameCache.level.meta.word_score)) * hot_word_score;
//					console.log("FRENZY INCREMENT: " + scoreIncrement);
				} else {
					scoreIncrement = GV.gameCache.level.meta.word_score * hot_word_score;
//					console.log("NORMAL INCREMENT: " + scoreIncrement);
				}
//				updateTopbar(score);
				animateScore(scoreIncrement);
				GV.gameCache.players[GV.playerCache.wow.player_id].score += scoreIncrement;
//				console.log("SCORE AFTER: " + GV.gameCache.players[GV.playerCache.wow.player_id].score);
			}

			//Change page number for list of words
			if ((GV.gameCache.found_word_count % GV.gameCache.level.meta.max_words_per_page) == 0 && GV.gameCache.found_word_count != GV.totalWords) {
				playAudio(GC.NEW_PAGE_AUDIO);
				GV.displayedPageNumber++;
				GV.loadNewPage = true;
			}

			if (GV.gameCache.found_word_count == GV.totalWords) {
				/*
				* Reset the wait related variable so game end waits for animation to complete before moving to the end screen.
				*/
				GV.waitForAnimationToEnd = false;
				if (GV.gameCache.level.meta.frenzy_allowed) {
					FrenzyKeeper.resetFrenzy();
				}
				console.log("GAME OVER!");
				GV.gameState = GC.GameState.GAME_OVER;
			}
		} else {
			playAudio(GC.WORD_INVALID_AUDIO);
		}

	}
}

function animateScore(scoreIncrement, onComplete) {
	if (GV.animation.scoreAnim.tween) {
		GV.animation.scoreAnim.tween.kill();
		GV.animation.scoreAnim.tween = null;
	}

	GV.animation.scoreAnim.score = GV.gameCache.players[GV.playerCache.wow.player_id].score;
	GV.animation.scoreAnim.tween =
		TweenLite.to(
			GV.animation.scoreAnim,
			0.5,
			{
				score: GV.gameCache.players[GV.playerCache.wow.player_id].score + scoreIncrement,
				onUpdate: function() {
					var score = GV.animation.scoreAnim.score.toFixed(0);
					//update the score
					$('#wowHeader .playerScore').text(score);

					var achievedTarget = Math.min(Math.floor((score/GV.gameCache.level.meta.score_target)*100), 100);
					$('#wowHeader .starProgressBar .theLightOne').css('width', achievedTarget+'%');
				},
				onComplete: onComplete || function(){
					GV.waitForAnimationToEnd = false;
				}
			}
		);
}

function updateTopbar(score) {
	//update the score
	$('#wowHeader .playerScore').text(score);
	//update level starbar progress

	var achievedTarget = Math.min(Math.floor((score/GV.gameCache.level.meta.score_target)*100), 100);
	$('#wowHeader .starProgressBar .theLightOne').css('width', achievedTarget+'%');
}

function gameOver(reason) {
	unbindTouchHandlers();
	//run remaining timer before reporting end of level
	var remainingSeconds = 0;
	if (GV.gameCache.level.meta.level_type != "SCORE_TRIAL" && GV.levelTimer) {
		remainingSeconds = GV.levelTimer.getRemainingSeconds();
	}

    // Pull out move data from all players data
    var game_data = {};
    for(var key in GV.gameCache.players) {
        for(var foundWord in GV.gameCache.players[key].found_words) {
            var foundWordData = GV.gameCache.players[key].found_words[foundWord];
            game_data[foundWord] = {found_by: key, found_time: foundWordData.found_time, rowId1: foundWordData.y1, rowId2: foundWordData.y2, colId1: foundWordData.x1, colId2: foundWordData.x2};
        }
    }

    game_data = JSON.stringify(game_data);

	board.killActiveLevelTimer();
	if (remainingSeconds > 0 && reason != GC.GameOverReason.KILL_MINUS_NINE) { //if there is time remaining and you're not abandoning, then update the score
		GV.levelTimer = new Countdown({
			seconds:remainingSeconds,  // number of seconds to count down
			interval: (1000/remainingSeconds),
			onUpdateStatus: function(sec) // callback for each second
			{
				var minutes = Math.floor(sec / 60);
				var seconds = sec % 60;
				$('#wowHeader .playerTimer .text').text(pad(minutes,2)+':'+pad(seconds,2));
				GV.gameCache.players[GV.playerCache.wow.player_id].score += GV.gameCache.level.meta.time_score + GC.getTimeBonus();

				$('#wowHeader .playerScore').text(GV.gameCache.players[GV.playerCache.wow.player_id].score);
				var achievedTarget = Math.min(Math.floor((GV.gameCache.players[GV.playerCache.wow.player_id].score/GV.gameCache.level.meta.score_target)*100), 100);
				$('#wowHeader .starProgressBar .theLightOne').css('width', achievedTarget+'%');
			},
			onCounterEnd: function() {
				console.log("counter ended");
				var campaign_level_instance_id = GV.gameCache.level.campaign_level_instance_id;
				var score_to_report = GV.gameCache.players[GV.playerCache.wow.player_id].score;
				GV.gameCache = null;
				GV.gameState = GC.GameState.NONE;
				GV.animation.scoreAnim.tween.kill();
				GV.animation.scoreAnim.tween = null;
				ZeCommunicator.endLevel(campaign_level_instance_id, score_to_report, reason, game_data);
			}
		});
		GV.levelTimer.start();
	} else { //if no remaining time, then just finish the level and show the end screen
		setTimeout(function(){
			ZeCommunicator.endLevel(GV.gameCache.level.campaign_level_instance_id, GV.gameCache.players[GV.playerCache.wow.player_id].score, reason, game_data);
			GV.gameCache = null;
			GV.gameState = GC.GameState.NONE;
		}, 1000);
	}
}
