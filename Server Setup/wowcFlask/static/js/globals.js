//Global Variables
(function (GV, $, undefined){
	GV.loadedInFB = false;
	GV.isSinglePlayer = true;
	//For MULTIPLAYER
	GV.userKey = '';
	GV.isOwner = false;

	GV.tileWidth = 30;
	GV.tileFontSize = 22;
	GV.totalWords = 0;
	GV.gameState = GC.GameState.NONE;
	GV.currentPlayerColor = '0000ff';
	GV.lineBeingDrawn = false;
	GV.canvasOffsetX = 0;
	GV.canvasOffsetY = 0;
	GV.levelTimer = null;
	GV.gridRows = -1;
	GV.gridColumns = -1;
	GV.displayedPageNumber = 0; //Words are divided in pages. This keeps index of current page.
	GV.foundWordAndCoordinates = {};
	GV.sortedPlayers = [];
	GV.audios = {};
	GV.waitForAnimationToEnd = false;
	GV.animation = {
		scoreAnim:{
			tween:null,
			score:0
		}
	};
	GV.loadNewPage = false;

	GV.gameCache = null;
	GV.playerCache = {
		facebook:{
			tries: 0,
		},
		wow:{}
	};
	GV.multiplayerGamePlayers = {}
	GV.facebookSignedRequest = "";

	/* SockJS */
	// Websocket callbacks:
	GV.gameKey = ''
	GV.webSocket = {};
	GV.webSocketOnopen = function() {
		GV.gameState = GC.GameState.SOCKET_CONNECTED;
		console.log("Connected to TORNADO SockJS...");

//		GV.webSocket.send(JSON.stringify((jQuery.validator.format(GC.COMMANDS.refresh_players, GV.gameKey))));
		ZeMultiCommunicator.connect(GV.gameKey);

//		//Take joiner directly to wait screen on connecting - auto click join button.
//		if($("#btnStart").text()==="Join")
//			$("#btnStart").click();
	};
	GV.webSocketOnmessage = function(event) {
//		console.log('GV.webSocketOnmessage');
		data = JSON.parse(event.data);
		ZeMultiCommunicator.processMessage(data);
	};
	GV.webSocketOnclose = function() {
//		console.log('GV.webSocketOnclose');
		if(GV.gameState != GC.GameState.SOCKET_DISCONNECTED) {
			//alert("This game has expired.");
			show_message(jQuery.validator.format("This game has expired."));
			//window.location = window.location.pathname;
		}
	};
	GV.webSocketOnerror = function(error) {
		console.log('GV.webSocketOnerror');
		console.log('Error: ' + error);
	};
}(window.GV = window.GV || {}, jQuery));