/*
	Self-Executing Anonymous Functions
	learn more about it from this awesome resource:
	http://appendto.com/2010/10/how-good-c-habits-can-encourage-bad-javascript-habits-part-1/
*/
(function (FrenzyKeeper, $, undefined) {
	//private variables start with var
	var lastWordFoundTime = null;
	var frenzyWordsFound = 0;
	var timerId = null;
	var frenzyTime = 15;
	var wordFindTime = 10;

	//public variables
	FrenzyKeeper.maxWordsToStartFrenzy = 3;
	FrenzyKeeper.FrenzyIsOn = false;
	FrenzyKeeper.State = {
		INACTIVE: 0,
		ACTIVE_NON_FRENZY: 1,
		ACTIVE_FRENZY: 2
	};
	FrenzyKeeper.frenzyState = FrenzyKeeper.State.INACTIVE;

	FrenzyKeeper.resetFrenzy = function () {
		frenzyWordsFound = 0;
		FrenzyKeeper.FrenzyIsOn = false;
		FrenzyKeeper.frenzyState = FrenzyKeeper.State.INACTIVE;
		FrenzyKeeper.updateTimer(-1);
	};

	FrenzyKeeper.isWordFoundInTen = function (frenzy_word_count) {
		if (FrenzyKeeper.FrenzyIsOn) {
			$("#frenzyProgressBar").width('100%');
			frenzyWordsFound = 0;
			FrenzyKeeper.frenzyState = FrenzyKeeper.State.ACTIVE_FRENZY;
			timer(frenzyTime);
			return;//Frenzy already on, no need to check
		}

		FrenzyKeeper.frenzyState = FrenzyKeeper.State.ACTIVE_NON_FRENZY;
		timer(wordFindTime);
		var d = new Date();
		var timeDiff = 0;
		if(frenzyWordsFound > 0)
			timeDiff = d.getTime()/1000 - lastWordFoundTime;
		lastWordFoundTime = d.getTime()/1000; //msec to sec

		if (frenzy_word_count == null) { //For single player campaign we are not sending any word count
			if(timeDiff <= 10) {
				frenzyWordsFound++;
			} else {
				frenzyWordsFound = 1;
			}
		} else { //For multiplayer games the frenzy word count is sent by the server
			frenzyWordsFound = frenzy_word_count;
		}

		//Now if we're ready tell the UI to mark frenzy ready stuff
		if (frenzyWordsFound == FrenzyKeeper.maxWordsToStartFrenzy) {
			FrenzyKeeper.FrenzyIsOn = true;
			timer(frenzyTime);
		}

		width = (frenzyWordsFound / FrenzyKeeper.maxWordsToStartFrenzy) * 100;
		$("#frenzyProgressBar").width(width+'%');
	};

	function timer(counter){
		if (timerId) {
			clearInterval(timerId);
		}
		timerId = setInterval(function() {
			counter--;
			if (counter >= 0) {
				FrenzyKeeper.updateTimer(counter);
			}
			if (counter === 0) {
				FrenzyKeeper.frenzyState = FrenzyKeeper.State.INACTIVE;
				FrenzyKeeper.updateTimer(-1);
				if (FrenzyKeeper.FrenzyIsOn) {
					FrenzyKeeper.FrenzyIsOn = false;
					liID = 'li' + GV.playerCache.facebook.profile.id;
					$("#"+liID).removeClass('frenzy');
				}
			}
		}, 1000);
	}

	FrenzyKeeper.updateTimer = function (counter) {
		if (counter == -1) {
			$("#timerSpan").html('00:00');
			$("#frenzyProgressBar").width('0%');
			clearInterval(timerId);
		}
		else{
			if (counter<10) {
				counter = "00:0"+counter;
			} else {
				counter = "00:"+counter;
			}
			$("#timerSpan").html(counter);
		}
	};
} (window.FrenzyKeeper = window.FrenzyKeeper || {}, jQuery));

