(function (board, $, undefined){
	board.useGameMapAndBuildLevel = function(message) {
		console.log('board.useGameMapAndBuildLevel');
		resetGlobals();
		GV.gameCache = message;
		game = message;
		if(GV.gameCache) {
			GV.gameState = GC.GameState.PLAYING;
			GV.totalWords = 0;
			for (var i in GV.gameCache.words) {
				GV.totalWords += GV.gameCache.words[i].length;
			}

			if (!GV.isSinglePlayer) {
				//TODO: Test and validate this. Then enable it.
//				DOING THIS LATER IN DISPLAY GAME PLAYERS
//				GV.gameCache.players = sortPlayers(GV.gameCache.players);
			} else {
				sortPlayers(0);
			}

			if (GV.gameCache.hasOwnProperty('displayed_page_number')) {
				GV.displayedPageNumber = GV.gameCache.displayed_page_number;
			}

			board.theBoard();
			if (GV.isSinglePlayer || (GV.gameCache.hasOwnProperty('status') && GV.gameCache.status != GC.GameState.GAME_OVER)) {
				bindTouchHandlers();
			}
			//playSound();
		}
	}

	board.theBoard = function () {
		buildTopbar();
		board.displayGamePlayers();
		board.displayLevel();
		computeAndSaveCanvasOffsets();
	}

	board.killActiveLevelTimer = function() {
		//
		if (GV.gameCache.level.meta.level_type != "SCORE_TRIAL" && GV.levelTimer) {
			$('#wowHeader .playerTimer .text').css('color', '');
			$('#wowHeader .playerTimer .text').removeClass('blink_me');
			GV.levelTimer.stop();
			GV.levelTimer = null;
		}
	}

	board.displayLevel = function() {
		drawLevelGrid();
		show('#progressBar');
	}

	board.displayPlayerScore = function() {
		$('#wowHeader .playerScore').text(GV.gameCache.players[GV.playerCache.wow.player_id].score);
	}

	board.displayGamePlayers = function() {
		if (!GV.isSinglePlayer) {
			GV.gameCache.players = sortPlayers(GV.gameCache.players);
			html = ''
			for (var i in GV.sortedPlayers) {
				var player = GV.gameCache.players[GV.sortedPlayers[i].player_id];
				if (player.joined) {

					if (!GV.multiplayerGamePlayers[player.facebook_id]) {
						GV.multiplayerGamePlayers[player.facebook_id] = {"profilePic":""}
					}
					var profilePic = GV.multiplayerGamePlayers[player.facebook_id].profilePic;
					if (profilePic && profilePic != '') {
						html += "<li>" +
									"<div class='gamePlayer'>" +
										"<img class='"+ (GV.sortedPlayers[i].player_id == GV.playerCache.wow.player_id?"playerMe":"") +"' width='35px' src="+ profilePic +">" +
										"<div class='gamePlayerScore'>"+player.score+"</div>" +
									"</div>" +
								"</li>";
					} else {
						html += "<li>" +
									"<div class='gamePlayer'>" +
										"<img class='"+ (GV.sortedPlayers[i].player_id == GV.playerCache.wow.player_id?"playerMe":"") + " fb" +player.facebook_id+"' width='35px' src='../../../static/images/multiplayer/icons/avatar.png'>" +
										"<div class='gamePlayerScore'>"+player.score+"</div>" +
									"</div>" +
								"</li>";
						getFbInvitedUsersInfo([player.facebook_id]);
					}
				}
			}
			$('#gamePlayers').html(html);
		}
	}

	/* CLEAR THE CANVAS THAT GETS THE TEMPORARY HIGHLIGHTS */
	board.drawGameCanvas = function(id) {
		var canvas2 = document.getElementById(id);
		var ctx2 = canvas2.getContext("2d");
		$("#canvas2").attr('width', GV.gameCache.game[0].length * GV.tileWidth);
		$("#canvas2").attr('height',GV.gameCache.game.length * GV.tileWidth);
		ctx2.clearRect(0, 0, canvas2.width, canvas2.height);
		return canvas2;
	}


	/****************** PRIVATE METHODS ********************/

	function computeAndSaveCanvasOffsets() {
		var canvas = document.getElementById("canvas");
		var element = canvas;
		var offsetX = -1, offsetY = -1;
		if (element.offsetParent !== undefined) {
			do {
				offsetX += element.offsetLeft;
				offsetY += element.offsetTop;
			} while ((element = element.offsetParent));
		}

		stylePaddingLeft = parseInt(document.defaultView.getComputedStyle(canvas, null)['paddingLeft'], 10)      || 0;
		stylePaddingTop  = parseInt(document.defaultView.getComputedStyle(canvas, null)['paddingTop'], 10)       || 0;
		styleBorderLeft  = parseInt(document.defaultView.getComputedStyle(canvas, null)['borderLeftWidth'], 10)  || 0;
		styleBorderTop   = parseInt(document.defaultView.getComputedStyle(canvas, null)['borderTopWidth'], 10)   || 0;
		// Some pages have fixed-position bars (like the stumbleupon bar) at the top or left of the page
		// They will mess up mouse coordinates and this fixes that
		var html = document.body.parentNode;
		htmlTop = html.offsetTop;
		htmlLeft = html.offsetLeft;

		offsetX += stylePaddingLeft + styleBorderLeft + htmlLeft;
		offsetY += stylePaddingTop + styleBorderTop + htmlTop;

		// Add padding and border style widths to offset
		// Also add the <html> offsets in case there's a position:fixed bar (like the stumbleupon bar)
		// This part is not strictly necessary, it depends on your styling
		GV.canvasOffsetX = offsetX;
		GV.canvasOffsetY = offsetY;

		GV.gridRows = GV.gameCache.game.length;
		GV.gridColumns = GV.gameCache.game[0].length;
	}

	function resetGlobals() {
		/* Globals */
		GV.gameCache = null;
		GV.foundWordAndCoordinates = {};
		GV.displayedPageNumber = 0;
		GV.sortedPlayers = [];
		if (GV.animation.scoreAnim.tween) {
			GV.animation.scoreAnim.tween.kill();
			GV.animation.scoreAnim.tween = null;
		}

		/* Other Stuff */
		FrenzyKeeper.resetFrenzy();
	}

	function buildTopbar() {
		//Profile Picture!
		$('#wowHeader img.playerPicture').attr('src',GV.playerCache.facebook.profile.profilePic);

		//Player Score!
		$('#wowHeader .playerScore').text('0'); //starting off at zero alright!
		$('#wowHeader .playerScore').css('min-width',$('#wowHeader .playerScore').width())

		//Level Timer!
		var minutes = Math.floor(GV.gameCache.level.meta.time_target / 60);
		var seconds = GV.gameCache.level.meta.time_target % 60;
		$('#wowHeader .playerTimer .text').text(pad(minutes,2)+':'+pad(seconds,2));

		//Enable level timer AND build star progress bar only if this is a single player game
		if (GV.isSinglePlayer) {
			board.killActiveLevelTimer();
			if (GV.gameCache.level.meta.level_type != "SCORE_TRIAL") {
				GV.levelTimer = new Countdown({
					seconds:GV.gameCache.level.meta.time_target,  // number of seconds to count down
					onUpdateStatus: function(sec) // callback for each second
					{
						var minutes = Math.floor(sec / 60);
						var seconds = sec % 60;
						if (sec < 11) {
							playAudio(GC.TIMER_ALERT_AUDIO);
							$('#wowHeader .playerTimer .text').css('color', 'red');
							$('#wowHeader .playerTimer .text').addClass('blink_me');
						}
						$('#wowHeader .playerTimer .text').text(pad(minutes,2)+':'+pad(seconds,2));
					},
					onCounterEnd: function(){ // final action
						$('#wowHeader .playerTimer .text').css('color', '');
						$('#wowHeader .playerTimer .text').removeClass('blink_me');
						gameOver(GC.GameOverReason.TIMED_OUT);
					}
				});
			}

			//Player star progress bar!
			$('#wowHeader .starProgressBar .theLightOne').css('width', '2%');
			$('#wowHeader #dividerStar1').css('left', GV.gameCache.level.meta.star1+'%')
			$('#wowHeader #dividerStar2').css('left', GV.gameCache.level.meta.star2+'%')
			$('#wowHeader #dividerStar3').css('left', GV.gameCache.level.meta.star3+'%')
		}
	}

	function buildDisplayedWords(found_words) {
		var wordlist = '<ul class="wordList">';
		for(w in GV.gameCache.words[GV.displayedPageNumber]) {
			wordObj = GV.gameCache.words[GV.displayedPageNumber][w];
			if (wordObj.is_hot && !found_words.hasOwnProperty(wordObj.word)) {
				wordlist += '<li><div class="word hotword">' + wordObj.word.toUpperCase() +'<span class="pts2">2x</span></div></li>'
			} else {
				var dict_class = GV.gameCache.level.meta.dictionary.split(".")[0];
				wordlist += '<li><div'+(found_words.hasOwnProperty(wordObj.word)? ' class="word foundWord s ' + dict_class + '"' :' class="word ' + dict_class + '"' )+'>' + wordObj.hint.toUpperCase()
				if (wordObj.is_hot) {
					wordlist += '<span class="pts2">2x</span></div></li>'
				} else {
					wordlist += '</div></li>'
				}
			}
		}
		wordlist +='</ul>';
		var wordCount = '';
		if(GV.totalWords > 0) {
			wordCount = '<span class="wordCount"> page: ' + (GV.displayedPageNumber+1) + '/' + GV.gameCache.words.length + '</span>';
		}

		wordlist += wordCount;
		$("#wordlist").html(wordlist);

		if (GV.displayedPageNumber == 0) { //Do this only once!
			$('#wordlist').css('height',$('#wordlist').outerHeight());
		}
	}

	function drawDisplayedWords() {
		//print wordlist and players
		var found_words = {};
		var foundWordCount = 0;
		for (playerIndex in GV.sortedPlayers) {
			player = GV.sortedPlayers[playerIndex].player_id;

			//Frenzy mode
			applyFrenzyStyle = '';

			if (FrenzyKeeper.FrenzyIsOn) {
				applyFrenzyStyle = ' frenzy ';
			}

			for(word in GV.gameCache.players[player].found_words) {
				word = GV.gameCache.players[player].found_words[word]["word"][0];
				var playerClass = 'colorme';
				if(player != GV.playerCache.wow.player_id) {
					playerClass = 'colorOther';//all other players are grey - CSS:colorOther
				}

				var playerColor = $('.'+playerClass).css('color');

				//TODO: if unable to find color from css using jquery, traverse stylesheets - need to find a better/optimal way.
				if(!playerColor) {
					playerColor = getCSS('color', playerClass);
				}

				var tmp_found_word_and_coordinates = {};
				tmp_found_word_and_coordinates[1] = GV.gameCache.players[player].found_words[word]["x1"];
				tmp_found_word_and_coordinates[2] = GV.gameCache.players[player].found_words[word]["y1"];
				tmp_found_word_and_coordinates[3] = GV.gameCache.players[player].found_words[word]["x2"];
				tmp_found_word_and_coordinates[4] = GV.gameCache.players[player].found_words[word]["y2"];
				tmp_found_word_and_coordinates[5] = playerColor;

				GV.foundWordAndCoordinates[foundWordCount] = tmp_found_word_and_coordinates;

				foundWordCount++;
				var wobj = GV.gameCache.players[player].found_words[word];
				found_words[word] = wobj;
			}
		}
		var margin_left = 0;
		var wordlist = null;
		if (GV.loadNewPage == true) {
			$("#wordlist").animate({
					opacity:0.05
				},
				200,
				function() {
					margin_left = $("#wordlist").css("margin-left");
					$("#wordlist").css("margin-left", 400);
					buildDisplayedWords(found_words);
			});
		} else {
			buildDisplayedWords(found_words);
		}

		if (GV.loadNewPage == true) {
			$("#wordlist").animate({
				marginLeft: 10,
				marginRight: 10,
				opacity:1
			}, 350, function() {
				GV.loadNewPage = false;
			});
			GV.loadNewPage = false;
		}
	}

	function drawLevelGrid() {
		drawDisplayedWords();

		//DRAW THE CANVAS
		canvas = document.getElementById("canvas");
		var canvasParentWidth = $('#gameCanvas').width();
		var canvasMaxWidth = Math.min(canvasParentWidth * 0.95, 400);

		var maxTiles = Math.max(GV.gameCache.game[0].length, GV.gameCache.game.length);
		var tileWidth = GV.tileWidth;
		GV.tileWidth = Math.floor(canvasMaxWidth / maxTiles);
		GV.tileFontSize = GV.tileFontSize * (GV.tileWidth / tileWidth);
		var ctx = canvas.getContext("2d");
		$("#canvas").attr('width', GV.gameCache.game[0].length * GV.tileWidth);
		$("#canvas").attr('height',GV.gameCache.game.length * GV.tileWidth);
		ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.font = ""+GV.tileFontSize+"px " + GC.CANVAS_FONT;
		if(GV.gameCache.level.meta.dictionary == "egyptian.dict") {
		    ctx.font = ""+GV.tileFontSize+"px " + GC.CANVAS_EGYPTIAN_FONT;
		}


		var canvasLeftPos = Math.floor((canvasParentWidth - canvas.offsetWidth)/2);

		var canvas2 = board.drawGameCanvas("canvas2");

		var position = $("#canvas2").position();
//		var rect = canvas2.getBoundingClientRect();
		$("#canvas").css("left", canvasLeftPos);
		$("#canvas").css("top", position.top);

		if (GV.gameCache.level.meta.frenzy_allowed) {
//			$("#frenzyDiv").css("width", $("#canvas2").css("width"));
		}
		var i = 0;
		for (var row in GV.gameCache.game)	{
			for (var col in GV.gameCache.game[row]) {
				//fill letters
				col = parseInt(col), row = parseInt(row)
				ctx.fillStyle = '#888';
				ctx.textAlign = 'center';
				ctx.textBaseline = 'middle';
				ctx.fillText(GV.gameCache.game[row][col].toUpperCase(), (col)*GV.tileWidth + GV.tileWidth/2, (row+1)*GV.tileWidth - GV.tileWidth/2);
			}
		}

		//MARK FOUND WORDS IF ALLOWED ON THIS LEVEL
		if (GV.gameCache.level.meta.highlight_allowed) {
			for(word in GV.foundWordAndCoordinates){
				drawFoundLine(GV.foundWordAndCoordinates[word][1], GV.foundWordAndCoordinates[word][2], GV.foundWordAndCoordinates[word][3], GV.foundWordAndCoordinates[word][4], GV.foundWordAndCoordinates[word][5]);
			}
		}
	}



}(window.board = window.board || {}, jQuery));