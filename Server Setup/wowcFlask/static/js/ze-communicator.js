/* AJAX METHODS BELOW THIS */

$(document).ready(function() {
	/*$('#spinner').bind('ajaxStart', function(){
		console.log("ajax call started.");
		//$(this).fadeIn('fast');
		$(this).show();
	}).bind('ajaxStop', function(){
		console.log("ajax call stopped.");
		//$(this).stop().fadeOut('fast');
		$(this).hide();
	});*/
	$(document).ajaxStart(function() {
		$("#spinner").show();
	});

	$(document).ajaxStop(function() {
		$("#spinner").hide();
	});
});

(function (ZeCommunicator, $, undefined){
	/*
		Checks if player exists in our system or not. If a player does not exist
		then creates a new record.
		Returns:
			playerId
	*/
	ZeCommunicator.getOrCreatePlayer = function(accessToken) {
		//saveMyFBInfoOnServer
		console.log("ZeCommunicator.getOrCreatePlayer");
		console.log(GV.facebookSignedRequest);
		$.post($SCRIPT_ROOT + "/player/get_or_create",
			{
				oauth_token: GV.playerCache.facebook.accessToken,
				profile_pic: GV.playerCache.facebook.profile.profilePic,
				fb_signed_request: JSON.stringify(GV.facebookSignedRequest),
			},
			function(response){
				try {
					GV.facebookSignedRequest = "";
					console.log("getCertPlayerFBInfo() response below");
					console.log(response);
					if(response.status === "SUCCESS") {
						GV.playerCache.wow.player_id = response.data.player_id
						if (response.data.game_key) {
							ZeCommunicator.handleMultiplayerGameRequest(response.data.game_key,[]);
						} else {
							ZeCommunicator.loadAllCampaigns();
						}
					} else {
						// TODO: NEAT ERROR
					}
				}
				catch(err) {
					console.log("catch(err)");
					console.log(err.message);
				}
			}
		);
	};

	ZeCommunicator.showLanding = function() {
		$.get($SCRIPT_ROOT + '/landing', {},
		function(response) {
			try {
				console.log("showLanding() response below")
				//console.log(response);
				if(response.status === "SUCCESS") {
					hide('#splash');
					var rootDiv = $("#wowContainer");
					rootDiv.html(response.data);
				} else {
					// TODO: NEAT ERROR
				}
			}
			catch(err) {
				console.log("catch(err)");
				console.log(err.message);
			}
		});
	};

	ZeCommunicator.loadAllCampaigns = function() {
		$.get($SCRIPT_ROOT + '/campaigns', {
			player_id: GV.playerCache.wow.player_id,
		}, function(response) {
			try {
				console.log("loadAllCampaigns() response below")
				//console.log(response);
				if(response.status === "SUCCESS") {
					hide('#splash');
					var rootDiv = $("#wowContainer");
					rootDiv.html(response.data);
				} else {
					// TODO: NEAT ERROR
				}
			}
			catch(err) {
				console.log("catch(err)");
				console.log(err.message);
			}
		});
	};

	ZeCommunicator.loadPlayerCampaign = function(campaign_id) {
		if (!GV.playerCache.wow.player_id) {
			alert("WoW! Campaign information not found!");
			return;
		}

		$.get($SCRIPT_ROOT + '/player/campaign', {
			player_id: GV.playerCache.wow.player_id,
			campaign_id: campaign_id
		}, function(response) {
			try {
				console.log("loadPlayerCampaign() response below")
				console.log(response);
				if(response.status === "SUCCESS") {
					$('#wowContainer').empty();
					var rootDiv = $("#wowContainer");
					rootDiv.html(response.data);
					//cache player's selected campaign_id
					GV.playerCache.wow.campaign_id = campaign_id;
				} else {
					// TODO: NEAT ERROR
				}
			}
			catch(err) {
				console.log("catch(err)");
				console.log(err.message);
			}
		});
	};

	ZeCommunicator.loadLevel = function(campaign_level_id) {

		$.get($SCRIPT_ROOT + '/player/campaign/start_level', {
			campaign_level_id: campaign_level_id,
			player_id: GV.playerCache.wow.player_id,
		}, function(response) {
			console.log("loadLevel() response below");
			console.log(response);
			if (response.status == "SUCCESS") {
				console.log(GV.playerCache);

				$('#wowContainer').empty();
				var rootDiv = $("#wowContainer");
				rootDiv.html(response.data.html);

				board.useGameMapAndBuildLevel(response.data.game_data)
			} else {
				// TODO: NEAT ERROR
			}
		});
	};

	ZeCommunicator.endLevel = function(campaign_level_instance_id, score, reason, game_play) {

		$.post($SCRIPT_ROOT + '/player/campaign/end_level', {
			campaign_level_instance_id: campaign_level_instance_id,
			player_id: GV.playerCache.wow.player_id,
			score: score,
			reason: reason,
			game_play: game_play
		}, function(response) {
			console.log("endLevel() response below");
			console.log(response);
			if(response.status === "SUCCESS") {
				$('#wowContainer').empty();
				var rootDiv = $("#wowContainer");
				rootDiv.html(response.data);
			} else {
				// TODO: NEAT ERROR
			}
		});
	};

	ZeCommunicator.getFriendsScores = function(campaign_level_id) {

		$.get($SCRIPT_ROOT + '/player/friends', {
			player_id: GV.playerCache.wow.player_id,
			campaign_level_id: campaign_level_id
		}, function(response) {
			console.log("getFriendsScores() response below");
			console.log(response);
			if(response.status === "SUCCESS") {
				var parentHeight = $('#levelEndDetails').height();

				var childrenHeight = 0;
				$("#levelEndDetails").children().filter(':not(.friendsRow)').each(function(){
    				childrenHeight = childrenHeight + $(this).outerHeight();
				});

				remainingHeight = (parentHeight - childrenHeight) * 0.95;
				$('.friendsScoreContainer').html(response.data);
//				$('.friendsScoreContainer').css('width','100%');
//				$('.friendsScoreContainer').css('height', remainingHeight);
//				$('.friendsScoreContainer').css('overflow-x', 'auto');
//				$('.friendsScoreContainer').css('overflow-y', 'hidden');
				show('#friendsScoreTable');
			} else {
				// TODO: NEAT ERROR
			}
		});
	};

	ZeCommunicator.loadMultiplayerScreen = function() {

		$.get($SCRIPT_ROOT + '/multiplayer', {
			player_id: GV.playerCache.wow.player_id
		}, function(response) {
			console.log("loadMultiplayerScreen() response below");
//			console.log(response);
			if(response.status === "SUCCESS") {
				console.log('Multiplayer Loaded!');
				$('#wowContainer').empty();
				var rootDiv = $("#wowContainer");
				rootDiv.html(response.data);
			} else {
				// TODO: NEAT ERROR
			}
		});
	};

	ZeCommunicator.savePlayerInvitations = function(facebook_ids) {

		$.get($SCRIPT_ROOT + '/player/savePlayerInvitations', {
			player_id: GV.playerCache.wow.player_id,
			facebook_ids: JSON.stringify(facebook_ids), //stringify the list so that flask doesn't blow up
		}, function(response) {
			console.log("savePlayerInvitations() response below");
			if(response.status === "SUCCESS") {
			} else {
				// TODO: NEAT ERROR
			}
		});
	};

	ZeCommunicator.getMultiplayerGameKey = function(callback) {

		$.get($SCRIPT_ROOT + '/multiplayer/getGameKey', {
			player_id: GV.playerCache.wow.player_id,
		}, function(response) {
			console.log("getMultiplayerGameKey() response below");
			if(response.status === "SUCCESS") {
				callback(response.data.game_key);
			} else {
				// TODO: NEAT ERROR
				callback(-99);
			}
		});

//		ZeCommunicator.handleMultiplayerGameRequest("ae8675d4-d955-11e4-a462-b8e856328886", ["1448150018786134"])
	};

	ZeCommunicator.handleMultiplayerGameRequest = function(gameKey, facebook_ids) {

		$.get($SCRIPT_ROOT + '/multiplayer/handleGameRequest', {
			player_id: GV.playerCache.wow.player_id,
			facebook_ids: JSON.stringify(facebook_ids), //stringify the list so that flask doesn't blow up
			game_key: gameKey,
		}, function(response) {
			console.log("handleGameRequest() response below");
			if(response.status === "SUCCESS") {
				if(response.gameKey != null || response.gameKey != '') {
					$('#wowContainer').empty();
					var rootDiv = $("#wowContainer");
					rootDiv.html(response.data);

					GV.multiplayerGamePlayers = {}
					GV.multiplayerGamePlayers[GV.playerCache.facebook.profile.id] = {"profilePic":""};
					for (var p in facebook_ids) {
						GV.multiplayerGamePlayers[facebook_ids[p]] = {"profilePic":""};
					}
				}
			} else {
				// TODO: NEAT ERROR
			}
		});
	};

	ZeCommunicator.getPlayerList = function(gameKey) {

		$.get($SCRIPT_ROOT + '/multiplayer/playerList', {
			game_key: gameKey,
		}, function(response) {
			if(response.status === "SUCCESS") {
				$('.waitingPlayersContainer').empty();
				var rootDiv = $(".waitingPlayersContainer");
				rootDiv.html(response.data);
			} else {
				// TODO: NEAT ERROR
			}
		});
	};

	ZeCommunicator.startMultiplayerGame = function(gameKey) {

		$.get($SCRIPT_ROOT + '/multiplayer/startGame', {}, function(response) {
			if(response.status === "SUCCESS") {
				$('#wowContainer').empty();
				var rootDiv = $("#wowContainer");
				rootDiv.html(response.data);
				board.useGameMapAndBuildLevel(GV.gameCache); //stored in ZeMultiCommunicator process message for force_start
			} else {
				// TODO: NEAT ERROR
			}
		});
	};

	ZeCommunicator.endMultiplayerGame = function(gameKey, gameData) {

		$.post($SCRIPT_ROOT + '/multiplayer/endGame', {
			player_id: GV.playerCache.wow.player_id,
			game_key: gameKey,
			game_data: JSON.stringify(gameData),
			multiplayer_game_players: JSON.stringify(GV.multiplayerGamePlayers)
		}, function(response) {
			if(response.status === "SUCCESS") {
				$('#wowContainer').empty();
				var rootDiv = $("#wowContainer");
				rootDiv.html(response.data);
			} else {
				// TODO: NEAT ERROR
			}
		});
	};

}(window.ZeCommunicator = window.ZeCommunicator || {}, jQuery));




