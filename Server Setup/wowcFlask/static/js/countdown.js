function Countdown(options) {
	var timer,
	instance = this,
	seconds = options.seconds || 10,
	updateStatus = options.onUpdateStatus || function () {},
	counterEnd = options.onCounterEnd || function () {},
	interval = options.interval || 1000;

	function decrementCounter() {
		updateStatus(seconds);
		if (seconds === 0) {
			counterEnd();
			instance.stop();
		}
		seconds--;
	}

	this.start = function () {
		clearInterval(timer);
		timer = 0;
		seconds = options.seconds;
		timer = setInterval(decrementCounter, interval);
	};

	this.stop = function () {
		clearInterval(timer);
	};

	this.getRemainingSeconds = function () {
		return seconds;
	};
}