$(document).ready(function() {
	//define console functions if we don't have access on some browsers
	if(typeof console === "undefined" || GC.CONSOLE_LOGGING_DISABLED) {
		console = {
			log: function() { },
			debug: function() { },
			info: function() { },
			warn: function() { },
			error: function() { }
		};
	}
});

function hexToR(h) {return parseInt((cutHex(h)).substring(0,2),16)}
function hexToG(h) {return parseInt((cutHex(h)).substring(2,4),16)}
function hexToB(h) {return parseInt((cutHex(h)).substring(4,6),16)}
function cutHex(h) {return (h.charAt(0)=="#") ? h.substring(1,7):h}

function hide(selector) {
		$(selector).addClass("hidden"); //apply the hidden class defined in bootstrap
}
function show(selector) {
		$(selector).removeClass("hidden"); //remove the hidden class defined in bootstrap
}

/*
* Reverse a string
*/
function reverse(s) {
  for (var i = s.length - 1, o = ''; i >= 0; o += s[i--]) { }
  return o;
}
/*
* Pad a number with leading zeros
*/
function pad(n, width, z) {
  z = z || '0';
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

/*
* Get attribute value for a CSS class
* The method creates a temp div, applies a class to it,
* gets the attribute value and returns it after removing the temp element from DOM
*/
function getCSS(prop, fromClass) {
	var $inspector = $("<div>").css('display', 'none').addClass(fromClass);
	$("body").append($inspector); // add to DOM, in order to read the CSS property
	try {
		return $inspector.css(prop);
	} finally {
		$inspector.remove(); // and remove from DOM
	}
}

/* Preload all audio files listed in GC.AUDIO_NAMES */
function loadAudios()
{
	$.each(GC.AUDIO_NAMES,function(){
		var tmp = new Audio();
		tmp.src = this;
		tmp.textContent = this;
		$(tmp).on('loadeddata',function(){
			var i = GC.AUDIO_NAMES.indexOf(this.textContent);
			GV.audios[GC.AUDIO_NAMES[i]] = tmp;
			GC.AUDIO_NAMES.splice(i,1);
			if (!GC.AUDIO_NAMES.length){
				console.log('Preloading done!');
			}
		});
	});
}
function playAudio(file) {
	GV.audios[file].play();
}

/*
* Get the text height, given a font type and size
*/
function getTextHeight(font) {

	var text = $('<span>Hg</span>').css({ fontFamily: font });
	var block = $('<div style="display: inline-block; width: 1px; height: 0px;"></div>');

	var div = $('<div></div>');
	div.append(text, block);

	var body = $('body');
	body.append(div);

	try {

		var result = {};

		block.css({ verticalAlign: 'baseline' });
		result.ascent = block.offset().top - text.offset().top;

		block.css({ verticalAlign: 'bottom' });
		result.height = block.offset().top - text.offset().top;

		result.descent = result.height - result.ascent;

	} finally {
		div.remove();
	}

	return result;
}

function sortPlayers(updatePlayers) {
	GV.sortedPlayers = [];
	allPlayer = [];
	if(updatePlayers == 0) {
		var playerNum = 0;
		for (plr in GV.gameCache.players) {
			allPlayer[plr] = playerNum;
			GV.sortedPlayers.push({'player_id':plr});
			playerNum++;
		}
	}
	else {
		gamePlayers = updatePlayers, tempMaxScore=0, tempPlayerId='';
		//var gamePlayerJSON = '{';
		var tempPlayers=[];
		var tempPlayerId;
		while(!$.isEmptyObject(gamePlayers)) {
			var len = Object.keys(gamePlayers).length;
			tempMaxScore = 0;
			for (plr in gamePlayers) {
				if(gamePlayers[plr].score >= tempMaxScore || len<=1) {
					tempMaxScore = gamePlayers[plr].score;
					tempPlayerId = plr;
				}
			}
			tempPlayers[tempPlayerId] = gamePlayers[tempPlayerId];
			delete gamePlayers[tempPlayerId];
			GV.sortedPlayers.push({'player_id':tempPlayerId});
		}

		return tempPlayers;
	}
}

/* POPUP BLOCKER CHECKING */
/* TEST IN ALL BROWSERS */
var popupBlockerChecker = {
	_isBlocked:false,
	check: function(popup_window, callback){
		var _scope = this;
		if (popup_window) {
			if(/chrome/.test(navigator.userAgent.toLowerCase())){
				setTimeout(function () {
					_scope._is_popup_blocked(_scope, popup_window);
					callback(popup_window);
					return;
				 },200);
			}else{
				popup_window.onload = function () {
					_scope._is_popup_blocked(_scope, popup_window);
					callback(popup_window);
					return;
				};
			}
		}else{
			_scope._displayError();
		}
		callback(popup_window);
		return;
	},
	_is_popup_blocked: function(scope, popup_window){
		if ((popup_window.innerHeight > 0)==false){ scope._displayError(); }
	},
	_displayError: function(){
		popupBlockerChecker._isBlocked = true;
//		alert("Popup Blocker is enabled! Please add this site to your exception list.");
	},
	getIsBlocked: function(){
		return popupBlockerChecker._isBlocked;
	}

};