import sqlalchemy.pool as pool
from datetime import datetime
import config
import mysql.connector
import logging
import config
#logging config
logging.basicConfig(filename=config.LOGGING['LOG_FILE'], level=logging.DEBUG, format=config.LOGGING['LOG_FORMAT'])

#PostGRESql
def connect_db():
	c = None
	try:
		DATABASE = config.DATABASE
		c = mysql.connector.connect(database=DATABASE["DATABASE"], user=DATABASE["USER"], host=DATABASE["HOST"], port=DATABASE["PORT"], password=DATABASE["PASSWORD"])
	except mysql.connector.DatabaseError, e:
		print 'Error %s' %e
	finally:
		return c

db_connection_swimming_pool = pool.QueuePool(connect_db , max_overflow=10, pool_size=5)


def get_connection_from_the_swimming_pool():
	con = db_connection_swimming_pool.connect()
	return con


def release_connection_back_to_the_swimming_pool(con):
	if con:
		con.close()

def get_multiplayer_game_for_key(gameKey):
	con = None
	try:
		con = get_connection_from_the_swimming_pool()
		cur = con.cursor(dictionary=True)
		cur.execute('''
			SELECT	mgi.status AS status,
					mgi.start_time AS start_time,
					mgi.end_time AS end_time,
					mgi.id as game_id,
					mgi.key as game_key,
					mg.game_data as game_data
			FROM multiplayer_game mg, multiplayer_game_instance mgi
			WHERE mg.id = mgi.multiplayer_game_id
			AND key = %s
		''', (gameKey,))
		return cur.fetchone()
	except mysql.connector.DatabaseError, e:
		if con:
			release_connection_back_to_the_swimming_pool(con)
		raise e
	finally:
		release_connection_back_to_the_swimming_pool(con)


def get_info():
	con = None
	try:
		con = get_connection_from_the_swimming_pool()
		cur = con.cursor(dictionary=True)
		cur.execute('''
			SELECT	*
			FROM INFO
			''')

		return cur.fetchone()
	except mysql.connector.DatabaseError, e:
		if con:
			release_connection_back_to_the_swimming_pool(con)
      		print e
  		print e
		raise e
	finally:
		release_connection_back_to_the_swimming_pool(con)


def get_multiplayer_game_players(gameKey):
	con = None
	data = None
	try:
		con = get_connection_from_the_swimming_pool()
		cur = con.cursor(dictionary=True)
		cur.execute('''
			SELECT	p.id as player_id,
					p.fb_id as facebook_id,
					p.profile_pic as profile_pic,
					mgp.joined as joined
			FROM 	multiplayer_game_instance mgi, multiplayer_game_player mgp, player p
			WHERE	KEY = %s
			AND mgi.id = mgp.multiplayer_game_instance_id
			AND mgp.player_id = p.id
			ORDER BY p.id''',(gameKey,))
		data = cur.fetchall()
	except mysql.connector.Error, e:
		print "Error %s" %e
		if con:
			release_connection_back_to_the_swimming_pool(con)
		raise e
	finally:
		release_connection_back_to_the_swimming_pool(con)
		return data

def save_multiplayer_game(gameKey, startTime, endTime, playerData):

	con = None
	data = None
	try:
		con = get_connection_from_the_swimming_pool()
		cur = con.cursor(dictionary=True)
		# also update game status
		cur.execute('''
			UPDATE multiplayer_game_instance SET start_time = %s,
					end_time = %s
			WHERE	KEY = %s
			''',(startTime, endTime, gameKey))

		cur.execute('SELECT id as id from multiplayer_game_instance where key = %s',(gameKey,))
		data = cur.fetchone()

		# Need to save player information as well.
		#SORT THEM BY SCORE AND SAVE ALL WITH POSITIONS
		for player in playerData:
			print playerData[player]
			if playerData[player]['joined']:
				cur.execute('''
				UPDATE multiplayer_game_player SET score = %s
				WHERE	multiplayer_game_instance_id = %s
				AND player_id = %s
				''',(playerData[player]['score'], data['id'], playerData[player]['id']))

		con.commit()
	except mysql.connector.Error, e:
		print "Error %s" %e
		if con:
			release_connection_back_to_the_swimming_pool(con)
		raise e
	finally:
		release_connection_back_to_the_swimming_pool(con)
		return data