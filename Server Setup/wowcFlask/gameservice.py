__author__ = 'omersaeed'

# Legend of the board ! ! ! ! !
# _ means board cell is permanently irrevokably blocked. CANNOT be used EVER
# + this is an intermediate state that allows the rest of the board making modes to use the cell of the frame
# 0 means regular cell, that will be filled by a character with the given score (0_9) of course
# ? means a cell that will take a PERMANENT state once it's used
# * this is a wild card. Will always remain as a wild card regardless of how many different characters use it.
# . hidden message char. there is a fixed character underneath it that will NEVER be shown
#   even after the correct word is matched around it.
# For the board the probability sets are defined like this
#	 [
#		 {u"symbol": '1', "count": 2, u"min_size": 2, u"max_size": 4},
#		 {u"symbol": '2', "count": 2, u"min_size": 2, u"max_size": 3},
#		 {u"symbol": '3', "count": 2, u"min_size": 2, u"max_size": 4},
#		 {u"symbol": '?', "count": 4, u"min_size": 1, u"max_size": 1},
#		 {u"symbol": '*', "count": 3, u"min_size": 1, u"max_size": 1}
#	 ]


import json
import uuid
import random
import logging

import model
import config
from datetime import datetime, timedelta
from gamemaker import GameMaker




#logging config
logging.basicConfig(filename=config.LOGGING['LOG_FILE'], level=logging.DEBUG, format=config.LOGGING['LOG_FORMAT'])

class GameService():
	"""
	A base request Handler providing user authentication.
	It also provides a render_default() method which passes arguments to render()
	with additional default arguments for the menu, user...
	"""
	def __init__(self):
		self.game_maker = GameMaker()

	def get_first_available_level_info(self, player_id, campaign_level_id):
		level_data = model.get_latest_played_level_info(player_id, campaign_level_id)
		if level_data is None or level_data['status'] != 'STARTED':
			last_played_level_instance_id = 0
			if level_data is not None:
				last_played_level_instance_id  = level_data['campaign_level_instance_id']

			level_data = model.get_next_available_level_info(campaign_level_id, last_played_level_instance_id)

		return level_data


	def publish_single_player_game(self, level_instance_data, player_id):
		game = json.loads(level_instance_data['game_data'])
		game['game_data']['players'] = self.create_player_data(player_id, True)
		game['game_data']['level']['campaign_level_instance_id'] = level_instance_data['campaign_level_instance_id']
		return game

	def create_player_data(self, player_id, is_owner):
		return {player_id: {'score': 0, 'is_owner': is_owner, 'found_words': {}, 'id': player_id, 'nick': "", 'profile_pic': "", 'is_frenzy': False, 'frenzy_countdown': 0, 'frenzy_word_count': 0, 'last_countdown': 0}}

	def create_game(self, level, dictionary):
		# Create this game
		data = {
			'max_words': level["max_words"],
			'min_length': level["word_min_length"],
			'max_length': level["word_max_length"],
			'dictionary': dictionary,
			'rows':level["rows"],
			'cols':level["columns"]
		}
		game = self.game_maker.get_game(data)
		game['game_instance'] = str(uuid.uuid1())
		game['level_meta'] = level

		all_words = [{"word": x, "hint": game['game']['words'][x][0], "disc": game['game']['words'][x][1], "is_hot": False} for x in game['game']['words']]

		hot_words = game['level_meta']['hot_words']
		while hot_words > 0:
			word = random.choice(all_words)
			if not word['is_hot']:
				word['is_hot'] = True
				hot_words -= 1

		page_size = game['level_meta']['max_words_per_page']
		paged_words = [all_words[i:i + page_size] for i in range(0, len(all_words), page_size)]

		return {
			'game_data': {
				'game': game['game']['map'],
				'board': game['game']['board']['map'],
				'words': paged_words,
				'level': {'meta': level}
			}
		}
