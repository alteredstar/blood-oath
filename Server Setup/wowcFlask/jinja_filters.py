__author__ = 'salmanqureshi'

ordinal_ = lambda n: "%d%s" % (n,"tsnrhtdd"[(n/10%10!=1)*(n%10<4)*n%10::4])

def pluralize(number, text, singular = '', plural = 's'):
	if number == 1:
		return str(number)+' '+text+singular
	else:
		return str(number)+' '+text+plural

def ordinal(number):
	return ordinal_(number)
