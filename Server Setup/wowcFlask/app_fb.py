from __future__ import division
import json
from bson import json_util
import logging
import constants
from gameservice import GameService

from flask import Response, Flask, jsonify, render_template, request, make_response
from flask.ext.assets import Environment, Bundle  # sudo pip install Flask-Assets
import facebook
import jinja_filters
import model
import requests
from utility import Utility

import urllib3.contrib.pyopenssl

urllib3.contrib.pyopenssl.inject_into_urllib3()

app = Flask(__name__)
app.config.from_object('config')
app.config.from_envvar('PRODUCTION_CONFIG', silent=True)

app.config['ASSETS_DEBUG'] = False  # set to false to bundle and compress js and css assets

assets = Environment(app)
js = Bundle('js/jquery/jquery-1.11.1.min.js',
            'js/jquery/plugins/jquery-ui-1.11.4.custom/jquery-ui.js',
            'js/jquery/plugins/jquery.validate.min.js',
            'js/jquery/plugins/jquery.query.string.js',
            'js/jquery/plugins/jquery.cookie.js',
            filters='jsmin',  # sudo pip install jsmin (Can use prepackaged rjsmin too)
            output='js/jquery/jquery_packed.js')
assets.register('js_jquery', js)

our_js = Bundle('js/board.js',
                'js/globals.js',
                'js/countdown.js',
                'js/ze-communicator.js',
                'js/ze-multi-communicator.js',
                'js/util.js',
                'js/frenzykeeper.js',
                'js/fb_social.js',
                'js/ui.js',
                'js/campaign.js',
                filters='jsmin',  # sudo pip install jsmin (Can use prepackaged rjsmin too)
                output='js/ours.js')
assets.register('js_ours', our_js)

css = Bundle("css/bootstrap.min.css",
             'js/jquery/plugins/jquery-ui-1.11.4.custom/jquery-ui.css',
             "css/jquery-ui-overrides.css",
             "css/main.css",
             "css/wowc.css",
             "css/campaign.css",
             "css/multiplayer.css",
             "rateit/src/rateit.css",
             "colorpicker/colorPicker.css",
             filters="cssmin",  # sudo pip install cssmin
             output="css/all.css"
             )
assets.register('css_all', css)

app.jinja_env.filters['pluralize'] = jinja_filters.pluralize
app.jinja_env.filters['ordinal'] = jinja_filters.ordinal


# the code below handles the addition of all custom jinja filters in the jinja env
# my_filters = {name: function
#				 for name, function in getmembers(jinja_filters.JinjaFilters)
#				 if isfunction(function)}
#
# app.jinja_env.filters.update(my_filters)

gameService = GameService()

# Probably Obsolete
# """
# @app.before_request
# def before_request():
# 	g.start = time.time()
# 	print request.url
#
# @app.teardown_request
# def teardown_request(exception=None):
# 	try:
# 		diff = time.time() - g.start
# 		print "************ IN TEARDOWN"
# 		print diff
# 	except Exception as e:
# 		logging.exception(e);
#
#
# @app.after_request
# def after_request(response):
# 	try:
# 		diff = time.time() - g.start
# 		print "************ IN AFTER REQUEST"
# 		print diff
# 	except Exception as e:
# 		logging.exception(e)
# 	finally:
# 		return response
# """


@app.route('/', methods=['GET', 'POST'])
def index():
    loaded_in_FB = False
    try:
        fb_request_stuff = ''
        if 'signed_request' and 'request_ids' in request.values:  # request coming from FB
            request_ids = request.values['request_ids']
            request_ids = request_ids.split(',')
            req_index = len(request_ids) - 1
            last_request_id = request_ids[req_index]
            fb_request_stuff = {
                'signed_request': request.values['signed_request'],
                'request_id': last_request_id
            }
            print fb_request_stuff
            loaded_in_FB = True
        elif 'signed_request' in request.values:
            loaded_in_FB = True

        template = render_template('wow.html', app_config=app.config, fb_request_stuff=fb_request_stuff,
                                   loaded_in_FB=loaded_in_FB)
    except Exception as e:
        template = "OOPS!"
        logging.exception(e)
    finally:
        return template


# FB Specific commands, do not follow the standard structure. Are just a workaround until FB gets its act together and fixes the plugin for Unity 5.0
@app.route('/FB/profile_picture/<fb_id>', methods=['GET'])
def get_profile_picture(fb_id):
    r = requests.get('https://graph.facebook.com/' + fb_id + '/picture?type=large', stream=True)
    fr = make_response(r.raw.read())
    fr.headers['Content-Type'] = r.headers['Content-Type']
    return fr


@app.route('/landing')
def landing():
        return jsonify({'name':'mannan','pass':'chussar'})


@app.route('/multiplayer', methods=['GET', 'POST'])
def multiplayer():
    status = constants.RESPONSE_STATUS.FAILURE
    try:
        data = render_template('partials/multiplayer/multiplayer.html', app_config=app.config)
        status = constants.RESPONSE_STATUS.SUCCESS
    except Exception as e:
        data = "OOPS!"
        logging.exception(e)
    finally:
        return jsonify({'command': '/multiplayer', 'status': status, 'data': data})


@app.route('/multiplayer/facebook_request_game_key', methods=['POST'])
def get_game_key():
    status = constants.RESPONSE_STATUS.FAILURE
    try:
        if 'oauth_token' in request.values and 'request_id' in request.values:
            graph = facebook.GraphAPI(request.values['oauth_token'])
            gameKey = graph.get_object(request.values['request_id'])['data']
            status = "SUCCESS"
            data = {'game_key': gameKey}
        else:
            raise Exception(constants.EXCEPTION.INVALID_REQUEST_SYNTAX)

    except Exception as e:
        data = {'message': 'Unable to get player', 'exception': e.message}
        logging.exception(data)

    return jsonify({'command': '/player/get_info', 'status': status, 'data': data})


@app.route('/player/get_or_create', methods=['POST'])
def player_cert():
    status = constants.RESPONSE_STATUS.FAILURE
    try:
        gameKey = ''
        if 'fb_signed_request' in request.values:
            # yeh cheetah har request mein available ho ga .. Chahay khali he ho!
            fb_signed_request = json.loads(request.values['fb_signed_request'])
            if fb_signed_request:
                signed_request = fb_signed_request['signed_request']
                request_id = fb_signed_request['request_id']
                if signed_request and request_id:  # agar empty string nahi hai tau
                    alternate_oauth_token = request.values['oauth_token']
                    gameKey = Utility.parseFacebookRequest(app, fb_signed_request['signed_request'], request_id,
                                                           alternate_oauth_token)

        if 'oauth_token' in request.values:
            oauth_token = request.values["oauth_token"]
            graph = facebook.GraphAPI(oauth_token)

            # get a long lived token
            extended_token = graph.extend_access_token(app.config['FB_APP_ID'], app.config['FB_APP_SECRET'])
            # replace the short lived token and save the long one in the DB
            oauth_token = extended_token['access_token']

            profile = graph.get_object('me')
        elif 'device_token' in request.values:
            # use device_token as the temporary fb_id to lookup device users's id
            profile = {
                'id': request.values["device_token"],
                'first_name': "HillyBilly",
                'last_name': "BagarBilla",
            }
            oauth_token = ''
        else:
            raise Exception(constants.EXCEPTION.INVALID_REQUEST_SYNTAX)

        # No use expecting the profile pic from the server. Should use the server api to get the latest URL
        profile_pic = ''
        if 'profile_pic' in request.values:
            profile_pic = request.values['profile_pic']
        email = ''
        if "email" in profile:
            email = profile["email"]

        player = model.get_player_with_fbid(profile["id"])
        if player is None:
            player_id = model.create_new_player(profile['id'], profile['first_name'], profile['last_name'],
                                                profile['first_name'], profile_pic, email, oauth_token, 'ACTIVE')
        elif player['status'] == constants.PLAYER_STATUS.INVITED:
            model.update_invited_player(player['player_id'], profile['first_name'], profile['last_name'],
                                        profile['first_name'], profile_pic, email, oauth_token, 'ACTIVE')
            player_id = player['player_id']
        else:
            player_id = player['player_id']
            if oauth_token != player['access_token']:
                model.update_player_token(player['player_id'], oauth_token)

        # If request contained a push_id update it as well
        if 'push_id' in request.values:
            model.update_player_push_id(player['player_id'], request.values['push_id'])

        status = "SUCCESS"
        data = {'player_id': player_id, 'game_key': gameKey}

    except Exception as e:
        print 'Error %s' % e
        data = {'message': 'Unable to get/create new player', 'exception': e.message}
        logging.exception(data)

    return jsonify({'command': '/player/get_or_create', 'status': status, 'data': data})


@app.route('/player/friends', methods=['GET'])
def friends_get():
    status = constants.RESPONSE_STATUS.FAILURE
    try:
        if 'player_id' and 'campaign_level_id' in request.values:
            data = model.get_player(request.values['player_id'])
            if data['player_id'] is not None:
                token = data['access_token']
                graph = facebook.GraphAPI(token)
                friends = {}
                if graph is not None:
                    friends = graph.get_connections("me", "friends")
                    temp_list = [str(friend['id']) for friend in friends['data']]
                    temp_list.append(data['facebook_id'])
                    friends = model.get_friends_scores(temp_list, request.values['campaign_level_id'])
                    status = "SUCCESS"

                # For web, translate the content properly. For now ASSUME that the list is actually the top 5 of em.
                if "is_device" not in request.values and status != constants.RESPONSE_STATUS.FAILURE:
                    print friends
                    data = render_template("partials/friends.html", friends=friends, me=data)
                else:
                    data = {'friends': friends}

            else:
                raise Exception(constants.EXCEPTION.INVALID_PLAYER_ID)
        else:
            raise Exception(constants.EXCEPTION.INVALID_REQUEST_SYNTAX)
    except Exception as e:
        data = {'message': 'Unable to get friends', 'exception': e.message}
        print 'EXCEPTION: %s' % e
        logging.exception(e)

    return jsonify({'command': '/player/friends', 'status': status, 'data': data})


@app.route('/player/get_info', methods=['GET'])
def player_get_info():
    status = constants.RESPONSE_STATUS.FAILURE
    try:
        if 'fb_id' in request.values:
            data = model.get_player_with_fb_id(request.values['fb_id'])
            if data['player_id'] is not None:
                status = "SUCCESS"
                data = {'player': data}
            else:
                raise Exception(constants.EXCEPTION.INVALID_PLAYER_ID)
        else:
            raise Exception(constants.EXCEPTION.INVALID_REQUEST_SYNTAX)

    except Exception as e:
        data = {'message': 'Unable to get player', 'exception': e.message}
        logging.exception(data)

    return jsonify({'command': '/player/get_info', 'status': status, 'data': data})


@app.route('/player/get', methods=['GET'])
def player_get():
    status = constants.RESPONSE_STATUS.FAILURE
    try:
        if 'player_id' in request.values:
            data = model.get_player(request.values['player_id'])
            if data['player_id'] is not None:
                status = "SUCCESS"
                data = {'player_id': data['player_id'], 'campaign_id': data['current_campaign_id']}
            else:
                raise Exception(constants.EXCEPTION.INVALID_PLAYER_ID)
        else:
            raise Exception(constants.EXCEPTION.INVALID_REQUEST_SYNTAX)

    except Exception as e:
        data = {'message': 'Unable to get player', 'exception': e.message}
        logging.exception(data)

    return jsonify({'command': '/player/get', 'status': status, 'data': data})


@app.route('/multiplayer/getGameKey', methods=['GET'])
def get_multiplayer_gamekey():
    status = constants.RESPONSE_STATUS.FAILURE
    try:
        game_meta = {
            'max_words': 30,
            'word_min_length': 4,
            'word_max_length': 10,
            'rows': 12,
            'columns': 12,
            'hot_words': 30 / 5,
            'max_words_per_page': 3,
            'highlight_allowed': True,
            'frenzy_allowed': True,
            'level_type': "SCORE_TRIAL"
        }
        game_data = gameService.create_game(game_meta, 'SAT')
        gameKey = model.create_new_multiplayer_game(json.dumps(game_data, default=json_util.default))
        # gameKey = 'ae8675d4-d955-11e4-a462-b8e856328886'
        # gameKey = 'a6eac94f-e5d3-11e4-b420-b8e856328886'
        data = model.get_multiplayer_game_for_key(gameKey)
        status = constants.RESPONSE_STATUS.SUCCESS
    except Exception as e:
        data = {'message': 'Unable to create new multiplayer game', 'exception': e.message}
        logging.exception(data)

    return jsonify({'command': '/multiplayer/getGameKey', 'status': status, 'data': {'game_key': data['game_key']}})


@app.route('/multiplayer/handleGameRequest', methods=['GET', 'POST'])
def handleGameRequest():
    status = constants.RESPONSE_STATUS.FAILURE
    players = []
    try:
        if 'player_id' and 'game_key' in request.values:
            gameKey = request.values['game_key']
            isOwner = False
            if 'facebook_ids' in request.values:
                facebook_ids = json.loads(request.values['facebook_ids'])
                if facebook_ids:
                    players = model.create_new_multiplayer_game_players(request.values['player_id'], gameKey,
                                                                        facebook_ids)
                    isOwner = True
                else:
                    model.update_multiplayer_game_player(request.values['player_id'], True)
                    players = model.get_multiplayer_game_players(gameKey)
            else:
                # TODO: Something when facebook_ids key is missing from payload.
                print 'testeststest'
        else:
            raise Exception(constants.EXCEPTION.INVALID_REQUEST_SYNTAX)

        data = {
            "players": players,
            "is_owner": isOwner
        }
        status = "SUCCESS"

    except Exception as e:
        print 'Error %s' % e
        data = {'message': 'Unable to get/create new player', 'exception': e.message}
        logging.exception(data)

    # For web, translate the content properly
    if "is_device" not in request.values and status != constants.RESPONSE_STATUS.FAILURE:
        data = render_template('partials/multiplayer/wait.html', app_config=app.config, game_key=gameKey,
                               is_owner=isOwner)

    return jsonify({'command': '/multiplayer/handleGameRequest', 'status': status, 'data': data})


@app.route('/player/savePlayerInvitations', methods=['GET', 'POST'])
def save_player_invitations():
    status = constants.RESPONSE_STATUS.FAILURE
    try:
        if 'player_id' and 'facebook_ids' in request.values:
            people = []
            facebook_ids = json.loads(request.values['facebook_ids'])
            # do this the pythonic way. Filter existing players out and then create new ones for the rest of the ids.
            # even though there shouldn't be any folks in this list who are in our system.
            for facebook_id in facebook_ids:
                player = model.get_player_with_fbid(facebook_id)
                if player is None:
                    profile = {
                        'id': facebook_id,
                        'first_name': "HillyBilly",
                        'last_name': "BagarBilla",
                    }
                    email = ''
                    profile_pic = ''
                    oauth_token = ''
                    player_status = constants.PLAYER_STATUS.INVITED
                    player_id = model.create_new_player(profile['id'], profile['first_name'], profile['last_name'],
                                                        profile['first_name'], profile_pic, email, oauth_token,
                                                        player_status)
                else:
                    player_id = player['player_id']

                people.append({facebook_id: player_id})

        else:
            raise Exception(constants.EXCEPTION.INVALID_REQUEST_SYNTAX)

        data = people
        status = "SUCCESS"

    except Exception as e:
        print 'Error %s' % e
        data = {'message': 'Unable to save player invitations', 'exception': e.message}
        logging.exception(data)

    return jsonify({'command': '/player/savePlayerInvitations', 'status': status, 'data': data})


@app.route('/multiplayer/startGame', methods=['GET'])
def startMultiplayerGame():
    status = constants.RESPONSE_STATUS.FAILURE

    try:
        data = render_template('partials/multiplayer/mp-board.html')
        status = constants.RESPONSE_STATUS.SUCCESS

    except Exception as e:
        print 'Error %s' % e
        data = {'message': 'Unable to start multiplayer game', 'exception': e.message}
        logging.exception(data)

    return jsonify({'command': '/multiplayer/startGame', 'status': status, 'data': data})


@app.route('/multiplayer/endGame', methods=['POST'])
def endMultiplayerGame():
    status = constants.RESPONSE_STATUS.FAILURE

    try:
        if 'game_key' and 'game_data' in request.values:
            gameKey = request.values['game_key']
            gameData = json.loads(request.values['game_data'])
            multiplayerGamePlayers = json.loads(request.values['multiplayer_game_players'])
            print gameKey
            print gameData
            print multiplayerGamePlayers

            players = model.get_multiplayer_game_players(gameKey)
            data = []
            for player in players:
                gamePlayer = gameData['players'][player['player_id']]
                dict = {
                    'player_id': player['player_id'],
                    'profile_pic': multiplayerGamePlayers[player['facebook_id']]['profilePic'],
                    'first_name': player['first_name'],
                    'score': gamePlayer['score'],
                    'facebook_id': player['facebook_id'],
                    'words_found': len(gamePlayer['found_words'])
                }
                data.append(dict)
            print data

            winner = data.pop(0)

            me = model.get_player(request.values['player_id'])
            friends = render_template('partials/friends.html', friends=data, me=me, multiplayer=True)
            data = render_template('partials/multiplayer/game-end.html', winner=winner, friends=friends)
            status = constants.RESPONSE_STATUS.SUCCESS

        # TODO: Something for device interaction
    except Exception as e:
        print 'Error %s' % e
        data = {'message': 'Unable to end multiplayer game', 'exception': e.message}
        logging.exception(data)

    return jsonify({'command': '/multiplayer/endGame', 'status': status, 'data': data})


@app.route('/multiplayer/playerList', methods=['GET'])
def playerList():
    status = constants.RESPONSE_STATUS.FAILURE
    people = []
    try:
        if 'game_key' in request.values:
            gameKey = request.values['game_key']
            results = model.get_multiplayer_game_players(gameKey)
            # not sure if we really need to translate the RealDict to python dict
            for p in results:
                people.append({'facebook_id': p['facebook_id'], 'profile_pic': p['profile_pic'], 'joined': p['joined']})
        else:
            raise Exception(constants.EXCEPTION.INVALID_REQUEST_SYNTAX)

        data = people
        status = "SUCCESS"

    except Exception as e:
        print 'Error %s' % e
        data = {'message': 'Unable to get player list', 'exception': e.message}
        logging.exception(data)

    # For web, translate the content properly
    if "is_device" not in request.values and status != constants.RESPONSE_STATUS.FAILURE:
        data = render_template('partials/multiplayer/waiting-players.html', people=people)

    return jsonify({'command': '/multiplayer/playerList', 'status': status, 'data': data, 'game_key': gameKey})


@app.route('/player/merge', methods=['GET'])
def player_merge():
    # The sync process is simple in a way that it essentially moves all gameplays from the old player id to the new one
    #  unifies the campaingns for both player ids
    #  and then updates the scores for each campaign
    #
    # In the end what is returned is the new player.
    status = constants.RESPONSE_STATUS.FAILURE
    try:
        if 'old_player_id' and 'new_player_id' in request.values:
            model.sync_players(request.values['old_player_id'], request.values['new_player_id'])
            status = "SUCCESS"
            data = {'player_id': request.values['new_player_id']}
        else:
            raise Exception(constants.EXCEPTION.INVALID_REQUEST_SYNTAX)

    except Exception as e:
        data = {'message': 'Unable to get player', 'exception': e.message}
        logging.exception(data)

    return jsonify({'command': '/player/get', 'status': status, 'data': data})


@app.route('/player/campaign', methods=['GET'])
def player_campaign():
    status = constants.RESPONSE_STATUS.FAILURE
    try:
        if "player_id" and "campaign_id" in request.values:
            data = model.get_player_campaign(request.values["player_id"], request.values["campaign_id"])
            if data is None:
                model.add_player_campaign(request.values["player_id"], request.values["campaign_id"])
            data = model.get_campaign_for_player(request.values["player_id"], request.values["campaign_id"])
            if len(data) < 0:
                raise Exception(constants.EXCEPTION.CAMPAIGN_DATA_NOT_FOUND)
            else:
                data = {"level_summary": data}
                status = constants.RESPONSE_STATUS.SUCCESS
        else:
            raise Exception(constants.EXCEPTION.INVALID_REQUEST_SYNTAX)

    except Exception as e:
        data = {'message': constants.RESPONSE_MESSAGE.PLAYER_NOT_FOUND, 'exception': e.message}
        logging.exception(data)

    # For web, translate the content properly
    if "is_device" not in request.values and status != constants.RESPONSE_STATUS.FAILURE:
        campaign_data = model.get_campaign_for_id(request.values['campaign_id'])
        campaign_name = campaign_data[0]['name']
        data = render_template("partials/campaign.html", levels=data['level_summary'], campaign_name=campaign_name)

    return jsonify({'command': '/player/campaign', 'status': status, 'data': data})


@app.route('/player/campaign_summary', methods=['GET'])
def player_campaign_summary():
    status = 'FAILURE'
    try:
        if "player_id" in request.values:
            data = model.get_campaign_summary_for_player(request.values["player_id"])
            if len(data) < 0:
                raise Exception("No Campaign Data Could be Found")
            else:
                data = {"campaign_summary": data}
                status = 'SUCCESS'
        else:
            raise Exception("Invalid Request Syntax Used")

    except Exception as e:
        data = {'message': 'Unable to get player campaign summary', 'exception': e.message}
        logging.exception(data)

    # For web, translate the content properly
    return jsonify({'command': '/player/campaign_summary', 'status': status, 'data': data})


@app.route('/player/campaign/start_level', methods=['GET'])
def start_level():
    status = constants.RESPONSE_STATUS.FAILURE
    try:
        if "campaign_level_id" and "player_id" in request.values:
            # verify that player has a level to play
            level_data = gameService.get_first_available_level_info(request.values["player_id"],
                                                                    request.values["campaign_level_id"])

            if level_data is not None and level_data['campaign_level_instance_id'] is not None:
                campaign_level_instance_id = level_data['campaign_level_instance_id']
            else:

                # get the dictionary from the level's campaign
                dictionary_name = model.get_campaign_dict_from_level_id(request.values["campaign_level_id"])[
                    'dictionary']

                # create a game level
                level_meta = model.get_campaign_level_config(request.values["campaign_level_id"])
                game_data = gameService.create_game(level_meta, dictionary_name)

                campaign_level_instance_id = model.add_level_instance(
                    request.values['campaign_level_id'],
                    json.dumps(game_data, default=json_util.default)
                )

            # Guaranteed to have a level :-)
            level_instance_data = model.get_campaign_level_instance(campaign_level_instance_id)

            # level_instance_data['game_data'] = json.loads(level_instance_data['game_data'])
            if level_data is None or level_data['status'] is None:
                model.start_campaign_level(request.values["player_id"],
                                           level_instance_data['campaign_level_instance_id'])

            data = gameService.publish_single_player_game(level_instance_data, request.values["player_id"])
            html = render_template("partials/board.html", level_meta=data['game_data']['level']['meta'])
            data['html'] = html
            status = constants.RESPONSE_STATUS.SUCCESS

        else:
            raise Exception(constants.EXCEPTION.INVALID_REQUEST_SYNTAX)

    except Exception as e:
        data = {'message': constants.RESPONSE_MESSAGE.LEVEL_DATA_NOT_FOUND, 'exception': e.message}
        logging.exception(data)

    return jsonify({'command': '/player/campaign/start_level', 'status': status, 'data': data})


@app.route('/player/campaign/end_level', methods=['POST'])
def end_level():
    status = constants.RESPONSE_STATUS.FAILURE
    campaign_level_config = {}
    try:
        if "campaign_level_instance_id" and "player_id" and "score" and "reason" and "game_play" in request.values:
            # calculate score and stars collected for this level
            # mark player level as COMPLETED!
            player_id = request.values["player_id"]
            campaign_level_instance_id = request.values["campaign_level_instance_id"]
            score = int(request.values["score"])
            game_play = request.values["game_play"]

            campaign_level_config = model.get_campaign_level_config_for_level_instance(player_id,
                                                                                       campaign_level_instance_id)

            # The level was not started with this player. Cannot end it without creating it first
            if campaign_level_config is None or len(campaign_level_config) == 0:
                model.start_campaign_level(player_id, campaign_level_instance_id)
                campaign_level_config = model.get_campaign_level_config_for_level_instance(player_id,
                                                                                           campaign_level_instance_id)

            score_target = campaign_level_config["score_target"]
            score_percentage = ((score / score_target) * 100)

            stars = 0
            # Sucks to find out that python doesn't have the ternary operator
            if score_percentage >= campaign_level_config["star3"]:
                stars = 3
            elif score_percentage >= campaign_level_config["star2"]:
                stars = 2
            elif score_percentage >= campaign_level_config["star1"]:
                stars = 1

            # get status and message for this level

            data = Utility.reason_to_message(int(request.values["reason"]), score, campaign_level_config)

            # Now update the players' stats for this level (instance)
            model.update_campaign_level_ended(player_id, campaign_level_instance_id,
                                              campaign_level_config["player_campaign_level_id"], data["status"], score,
                                              stars, game_play)

            campaign_data = model.get_campaign_for_id(campaign_level_config['campaign_id'])
            data["campaign_name"] = campaign_data[0]['name']
            data["score"] = score
            data["stars"] = stars

            status = constants.RESPONSE_STATUS.SUCCESS
        else:
            raise Exception(constants.EXCEPTION.INVALID_REQUEST_SYNTAX)

    except Exception as e:
        data = {
            'message': 'Oops! WoW just failed to mark your level as complete. You might have to play this one again.',
            'exception': e.message}
        logging.exception(data)

    if "is_device" not in request.values and status != constants.RESPONSE_STATUS.FAILURE:
        data = render_template("partials/level-end.html", end_payload=data, level_meta=campaign_level_config,
                               level_cleared=(True if status == constants.RESPONSE_STATUS.SUCCESS else False))

    return jsonify({'command': '/player/campaign/end_level', 'status': status, 'data': data})


@app.route('/campaigns', methods=['GET'])
def campaign():
    status = 'FAILURE'
    try:
        data = {"campaigns": model.get_campaigns()}
        status = "SUCCESS"
    except Exception as e:
        data = {'message': 'Unable to get campaigns', 'exception': e.message}
        logging.exception(data)

    # For web, translate the content properly
    if "is_device" not in request.values and status != constants.RESPONSE_STATUS.FAILURE:
        player_campaign_stars = {}
        if "player_id" in request.values:
            tempdict = model.get_campaign_summary_for_player(request.values["player_id"])

            # test = dict((tempdict['campaign_id'], tempdict['total_stars']) for (tempdict['campaign_id'], tempdict['total_stars']) in tempdict)

            player_campaign_stars = {x['campaign_id']: x['total_stars'] for x in tempdict}

        # For web ONLY render Large Campaigns
        large_campaigns = [x for x in data["campaigns"] if x["device_type"] == "LARGE"]
        data = render_template("partials/all-campaigns.html", campaigns=large_campaigns,
                               player_campaign_stars=player_campaign_stars)

    return jsonify({'command': '/campaigns', 'status': status, 'data': data})


@app.route('/payments', methods=['GET', 'POST'])
def paymentsCallback():
    hub_challenge = 'Hello World!'
    if 'hub.mode' and 'hub.challenge' in request.values:
        hub_mode = request.values['hub.mode']
        hub_challenge = request.values['hub.challenge']
        verification_token = request.values['hub.verify_token']
        if verification_token == app.config['FB_VERIFICATION_TOKEN']:
            print "TOKEN VERIFIED, NOW GO DO SOMETHING ELSE!"
    return hub_challenge

if __name__ == '__main__':
    app.run(host='localhost', port=9001)
