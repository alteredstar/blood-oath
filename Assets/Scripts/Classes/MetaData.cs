﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MetaData {

    public float hitpoints;
    public float productionRate;
    public float fire_rate;
    public Dictionary<string,int> storage;
    public Dictionary<string,int> damage; // Or Effect Type heal / Slow / Fast / Jump etc etc
    public Dictionary<string,int> armor;
    public int productionQueue;
    public float movementSpeed;
    public float trainingCost;
    public float damageArea; //(or Effect Area)
    public string spritePrefix;

}
