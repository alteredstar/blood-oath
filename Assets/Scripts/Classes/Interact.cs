﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;

public static class Interact {

    public static int building_movement = 0;

    public static GameObject[] tiles = GameObject.FindGameObjectsWithTag("InnerTilestag");
    public static int css = 0;


    public static void Start()
    {

        populate_universe();

    }

    public static List<Item> village = new List<Item>();
    public static Dictionary<string, Dictionary<int, MetaData>> metaUniverse;

    public static void populate_universe()
    {
        TextAsset t = (TextAsset)Resources.Load("Village", typeof(TextAsset));
        village = JsonConvert.DeserializeObject<List<Item>>(t.text);

        TextAsset s = (TextAsset)Resources.Load("MetaUniverse", typeof(TextAsset));
        metaUniverse = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<int, MetaData>>>(s.text);

    }

    public static void update_Server()
    {
       
        string json = JsonConvert.SerializeObject(village);                  
        System.IO.File.WriteAllText(@"Assets//Resources//Village.json", json);

    }

    public static Item add_to_universe(Vector3 loc,string name)
    {
        Item new_item = new Item();
        new_item.WorldLocation.x = loc.x;
        new_item.WorldLocation.y = loc.y;
        new_item.WorldLocation.z = loc.z;
        new_item.level = 1;

        new_item.GUID_itemId = village.Count + 1;
        new_item.type = name;
        village.Add(new_item);
        return new_item;
    }

    public static void Update_Universe(int id,int level,Location world_location)
    {
        village[id].level = level;
        village[id].WorldLocation = world_location;

    }



}
