﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Item {

    public int GUID_itemId;
    public int level;
    public string type;
    public Location WorldLocation;
    public int creation_time = 100;

    public Item()
    {
        WorldLocation = new Location();
        WorldLocation.x = 0;
        WorldLocation.y = 0;
        WorldLocation.z = 0;
    }

}

