﻿using UnityEngine;
using System.Collections;
using Pathfinding;

public class ASTAR_FINDER : MonoBehaviour {

    public Vector3 targetPosition;
    private Seeker seeker;
    public Path path;
    public float speed = 1;
    public float nextWaypointDistance = 3;
    private int currentWaypoint = 0;
    int ssp = 8;

    public void Start()
    {
        seeker = GetComponent<Seeker>();
        targetPosition = transform.position;
        getrandom();
    }
    public void getrandom()
    {
        Vector3 prevpos = targetPosition;

        targetPosition = Interact.tiles[ssp].transform.position;

        seeker.StartPath(prevpos, targetPosition, OnPathComplete);
        ssp++;         
    }
    public void OnPathComplete(Path p)
    {
        if (!p.error)
        {
            path = p;
            currentWaypoint = 0;
        }
 
    }
    public void Update()
    {
        if (path == null)
        {
            return;
        }
        if (currentWaypoint >= path.vectorPath.Count)
        {
           // currentWaypoint = 0;
            getrandom();
            return;
        }

        Vector3 dir = (path.vectorPath[currentWaypoint] - transform.position);
        dir *= speed * Time.deltaTime;
        transform.Translate(dir);

        if (Vector3.Distance(transform.position, path.vectorPath[currentWaypoint]) < nextWaypointDistance)
        {
            currentWaypoint++;
            return;
        }
    
        }

}
