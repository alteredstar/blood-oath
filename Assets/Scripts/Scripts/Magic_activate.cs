﻿using UnityEngine;
using System.Collections;


public class Magic_activate: MonoBehaviour {
	
	GameObject mouse_trails;
	int check = 0;
	void Start() {
        mouse_trails = GameObject.Find("MouseTrack");
    }
	public void activate() {
		if (check == 0) {
			check = 1;

            Interact.building_movement = 2;
        } else {
            Interact.building_movement = 0;
			check = 0;
		}
	}
	
 
	// Update is called once per frame
	void Update() {
 
		if (check == 1) {
			if(Input.GetMouseButton(0)){
			Vector3 vec3 = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			vec3.z = -2.5f;
			mouse_trails.transform.position = vec3;
			}
	}
	}
}