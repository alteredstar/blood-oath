﻿using UnityEngine;
using System.Collections;

public class Building_Movement: MonoBehaviour {
	
	public int chk = 0;
	Vector3 vect;
	Color clr;
    Vector3 pre_vect;
    int first_clk = 0;
    int build_over_chk = 0;
    public Item Itype = new Item();

	void Start() {
	clr = this.GetComponent<SpriteRenderer> ().color;
    }
    void LaunchProjectile()
    {
        TextMesh text = (TextMesh)transform.Find("Object_Stats/Health").GetComponent(typeof(TextMesh));
        if (Itype.creation_time > 0)
        {
            Itype.creation_time = Itype.creation_time - 1;
            text.text = (100 - Itype.creation_time).ToString() + "%";
            Interact.update_Server();
        }
    }
    void Update() {

        Invoke("LaunchProjectile", 10);


        if (chk == 0 ) {
			set_pos();
			vect.z = -1;

            if (chl == 0)
            {

                vect.z = -2;
                this.transform.position = vect;
            }
		}
		
	}
	int chl = 0;
	public void set_pos() {
		Vector2 worldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		int NPCMask = 1 << 9; 

		RaycastHit2D hit = Physics2D.Raycast(worldPoint, Vector3.zero,Mathf.Infinity,NPCMask);
		if (hit.collider != null) {
            if (hit.collider.tag.Equals("Building")) {
                vect.z = -2;
                chl = 1;
				return;
			}
			chl = 0;
            vect = hit.collider.transform.position;
			 
		}
	}
	void OnMouseDown() {
        if (first_clk == 0)
        {
            pre_vect = this.transform.position;
        }
        first_clk = 1;
        if (Interact.building_movement == 0) {

                Interact.building_movement = 1;
			    chk = 0;
                Vector3 vec = this.transform.position;
                vec.z = -2;
                this.transform.position = vec;
         }
	}

		void OnMouseUp() {

        first_clk = 0;
        if (Interact.building_movement == 1) {

                Interact.building_movement = 0;
			    chk = 1;
            if (chl == 0)
            {
                Vector3 vec = this.transform.position;
                vec.z = -1;
                this.transform.position = vec;
                Itype.WorldLocation.x = vect.x;
                Itype.WorldLocation.y = vect.y;
                Itype.WorldLocation.z = vect.z;
                Interact.update_Server();
            }
            if (build_over_chk == 1)
                this.transform.position = pre_vect;
                this.GetComponent<SpriteRenderer>().color = clr;
                Itype.WorldLocation.x = pre_vect.x;
                Itype.WorldLocation.y = pre_vect.y;
                Itype.WorldLocation.z = pre_vect.z;
        }
    }

    void OnTriggerEnter2D(Collider2D coll) {

		if (chk == 0) {
			if (coll.gameObject.tag.Equals ("Building")){
                build_over_chk = 1;
                this.GetComponent<SpriteRenderer> ().color = Color.red;
			}
		 }
	}
	void OnTriggerExit2D(Collider2D coll) {

        if (chk == 0) {
			{
				if (coll.gameObject.tag.Equals ("Building")){
                    build_over_chk = 0;
                this.GetComponent<SpriteRenderer> ().color = clr;
				}
			}
		}
}

	
}