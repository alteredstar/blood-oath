﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Building_Placer : MonoBehaviour {

	// Use this for initialization
	void Start() {


	}

    Transform get_transform(string BLD_ID)
    {
        GameObject prefab = Instantiate(Resources.Load(BLD_ID)) as GameObject;
        Vector3 vec3 = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        vec3.z = -2;
        transform.position = vec3;
        prefab.transform.position = vec3;
        Item item = Interact.add_to_universe(prefab.transform.position, BLD_ID);
        Debug.Log(item.creation_time);
        prefab.GetComponent<Building_Movement>().Itype = item;
        Interact.update_Server();
        return prefab.transform;
    }

	public void building_object(string BLD_ID) {

          get_transform(BLD_ID);


    }

}