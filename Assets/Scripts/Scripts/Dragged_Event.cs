﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Dragged_Event: MonoBehaviour {
	private float zoom; // the current camera zoom factor
	
	private float currentFingerDistance;
	private float previousFingerDistance;
	
	
	void Start() {
		
	}
	private Vector3 lastPosition;
	public float mouseSensitivity = 1;
	
	private float cam_wth;
	private float cam_hth;
	
	public void drag_mousecamera() {
		
		if (Input.GetMouseButtonUp(0)) ssp2 = 1;
		
		
		if (ssp2 == 1) {
			return;
		}
		
		if (Input.GetMouseButtonDown(0)) {
			lastPosition = Input.mousePosition;
		}
		if (Input.GetMouseButton(0)) {
			Vector3 delta = Input.mousePosition - lastPosition;
			
			transform.Translate(-delta.x / 20 * (mouseSensitivity), -delta.y / 20 * (mouseSensitivity), 0);
			lastPosition = Input.mousePosition;
		}
	}
	
	
	
	int ssp2 = 0;
	// conditions keep the camera from going off-map, leaving a margin for zoom-in/out
	void FixedUpdate() {
		int max_bound = 0;
		if (transform.position.y > 36 - ssp1) {
			if (ssp2 != 0) transform.position += new Vector3(0, -1, 0);
			max_bound = 1;
		}
		if (transform.position.x < -140 + ssp) {
			if (ssp2 != 0) transform.position += new Vector3(1, 0, 0);
			max_bound = 1;
		}
		if (transform.position.y < -32 + ssp1) {
			if (ssp2 != 0) transform.position += new Vector3(0, 1, 0);
			max_bound = 1;
		}
		if (transform.position.x > -57 - ssp) {
			if (ssp2 != 0) transform.position += new Vector3(-1, 0, 0);
			max_bound = 1;
		}
		if (max_bound == 0) ssp2 = 0;
		
	}
	double ssp = 0;
	double ssp1 = 0;
	public void zoominout() {
		
		if (Input.GetAxis("Mouse ScrollWheel") > 0) // forward
		{
			if (Camera.main.orthographicSize < 23.4988) {
				Camera.main.orthographicSize = Camera.main.orthographicSize + 1;
				ssp = ssp + 2.5;
				ssp1 = ssp1 + 1;
				ssp2 = 1;
			}
		}
		
		if (Input.GetAxis("Mouse ScrollWheel") < 0) // back
		{
			if (Camera.main.orthographicSize > 5.498804) {
				Camera.main.orthographicSize = Camera.main.orthographicSize - 1;
				ssp = ssp - 2.5;
				ssp1 = ssp1 - 1;
				ssp2 = 1;
			}
		}
		
		
	}
	void Update() {

		 if (Interact.building_movement == 0) {
			drag_mousecamera ();
			zoominout ();
		 }
		
	}
	
}